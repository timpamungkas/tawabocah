package com.tawabocah.pdf;

import java.util.Map;

import com.google.common.collect.Maps;
import com.tawabocah.util.DropboxUtil;

/**
 * Wrapper bean (POJO only). Each pdf asset will be mapped to dropbox's :
 * <ul>
 * <li>asset (pdf) path</li>
 * <li>thumbnail path</li>
 * <li>image path</li>
 * </ul>
 * 
 * @author Timotius Pamungkas
 *
 */
public class PDFToDropboxContainer {

	private String assetDbxPath;
	/**
	 * Path to pdf dropbox shared link. Not using {@link Map} to avoid multiple
	 * pdf assignment. Should only contains one asset pdf.
	 */
	private String assetDbxSharedLinkUrl;

	private String assetOriginalPath;
	
	/**
	 * Path to pdf file (copy)
	 */
	private String assetPath;

	/**
	 * Image map, because one pdf can contains more than one page, and each page
	 * will be converted into thumbnail & image. The key is image path, value is
	 * dropbox shared link (shared link comes later)
	 */
	private Map<String, String> imageMap;

	/**
	 * Thumbnail map, because one pdf can contains more than one page, and each
	 * page will be converted into thumbnail & image. The key is image path,
	 * value is dropbox shared link (shared link comes later)
	 */
	private Map<String, String> thumbnailMap;

	public PDFToDropboxContainer() {
		this.imageMap = Maps.newLinkedHashMap();
		this.thumbnailMap = Maps.newLinkedHashMap();
	}

	public void clearImages() {
		imageMap.clear();
	}

	public void clearThumbs() {
		thumbnailMap.clear();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PDFToDropboxContainer other = (PDFToDropboxContainer) obj;
		if (assetDbxPath == null) {
			if (other.assetDbxPath != null)
				return false;
		} else if (!assetDbxPath.equals(other.assetDbxPath))
			return false;
		if (assetDbxSharedLinkUrl == null) {
			if (other.assetDbxSharedLinkUrl != null)
				return false;
		} else if (!assetDbxSharedLinkUrl.equals(other.assetDbxSharedLinkUrl))
			return false;
		if (assetPath == null) {
			if (other.assetPath != null)
				return false;
		} else if (!assetPath.equals(other.assetPath))
			return false;
		if (imageMap == null) {
			if (other.imageMap != null)
				return false;
		} else if (!imageMap.equals(other.imageMap))
			return false;
		if (thumbnailMap == null) {
			if (other.thumbnailMap != null)
				return false;
		} else if (!thumbnailMap.equals(other.thumbnailMap))
			return false;
		return true;
	}

	public String getAssetDbxPath() {
		return assetDbxPath;
	}

	public String getAssetDbxSharedLinkUrl() {
		return DropboxUtil.parseDropboxUserContent(assetDbxSharedLinkUrl);
	}

	public String getAssetOriginalPath() {
		return assetOriginalPath;
	}

	public String getAssetPath() {
		return assetPath;
	}

	public Map<String, String> getImageMap() {
		return imageMap;
	}

	public Map<String, String> getThumbnailMap() {
		return thumbnailMap;
	}

	@Override
	public int hashCode() {
		final int prime = 7;
		int result = 1;
		result = prime * result + ((assetDbxPath == null) ? 0 : assetDbxPath.hashCode());
		result = prime * result + ((assetDbxSharedLinkUrl == null) ? 0 : assetDbxSharedLinkUrl.hashCode());
		result = prime * result + ((assetPath == null) ? 0 : assetPath.hashCode());
		result = prime * result + ((imageMap == null) ? 0 : imageMap.hashCode());
		result = prime * result + ((thumbnailMap == null) ? 0 : thumbnailMap.hashCode());
		return result;
	}

	/**
	 * Put to image {@link Map}
	 * 
	 * @param key
	 * @param value
	 */
	public void putImage(String key, String value) {
		this.imageMap.put(key, DropboxUtil.parseDropboxUserContent(value));
	}

	/**
	 * Put to image {@link Map}
	 * 
	 * @param key
	 * @param value
	 */
	public void putThumb(String key, String value) {
		this.thumbnailMap.put(key, DropboxUtil.parseDropboxUserContent(value));
	}

	public void setAssetDbxPath(String assetDbxPath) {
		this.assetDbxPath = assetDbxPath;
	}

	public void setAssetDbxSharedLinkUrl(String assetDbxSharedLinkUrl) {
		this.assetDbxSharedLinkUrl = assetDbxSharedLinkUrl;
	}

	public void setAssetOriginalPath(String assetOriginalPath) {
		this.assetOriginalPath = assetOriginalPath;
	}

	public void setAssetPath(String assetPath) {
		this.assetPath = assetPath;
	}

	public void setImageMap(Map<String, String> imageMap) {
		this.imageMap = imageMap;
	}

	public void setThumbnailMap(Map<String, String> thumbnailMap) {
		this.thumbnailMap = thumbnailMap;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PDFToDropboxContainer [assetDbxPath=").append(assetDbxPath).append(", assetDbxSharedLinkUrl=")
				.append(assetDbxSharedLinkUrl).append(", assetPath=").append(assetPath).append(", imageMap=")
				.append(imageMap).append(", thumbnailMap=").append(thumbnailMap).append("]");
		return builder.toString();
	}

}
