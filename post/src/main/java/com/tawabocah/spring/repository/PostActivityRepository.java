package com.tawabocah.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PostActivity;

@Repository
public interface PostActivityRepository extends PagingAndSortingRepository<PostActivity, Long> {

}
