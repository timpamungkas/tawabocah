package com.tawabocah.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PostArticle;

@Repository
public interface PostArticleRepository extends PagingAndSortingRepository<PostArticle, Long> {

}
