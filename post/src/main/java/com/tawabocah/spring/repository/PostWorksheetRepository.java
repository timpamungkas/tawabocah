package com.tawabocah.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PostWorksheet;

@Repository
public interface PostWorksheetRepository extends PagingAndSortingRepository<PostWorksheet, Long> {

}
