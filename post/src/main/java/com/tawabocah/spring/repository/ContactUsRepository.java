package com.tawabocah.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.Message;

@Repository
public interface ContactUsRepository extends CrudRepository<Message, Long> {

}
