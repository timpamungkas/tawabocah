package com.tawabocah.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PostLessonPlan;

@Repository
public interface PostLessonPlanRepository extends PagingAndSortingRepository<PostLessonPlan, Long> {

}
