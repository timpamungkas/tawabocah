package com.tawabocah.controller;

import java.util.Calendar;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tawabocah.hibernate.domain.Message;
import com.tawabocah.service.ContactUsService;

@RestController
@RequestMapping(path = "/api/v1")
public class ContactUsRestController {

	@Autowired
	private ContactUsService contactUsService;

	@RequestMapping(path = "/contact/save", method = RequestMethod.POST)
	private boolean contactUsPost(@RequestBody Message message) {
		message.setCreationDate(Calendar.getInstance().getTime());
		boolean isSaved = false;

		try {
			isSaved = contactUsService.saveAndSendMail(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return isSaved;
	}

}
