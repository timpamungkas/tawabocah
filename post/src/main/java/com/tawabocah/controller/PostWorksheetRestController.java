package com.tawabocah.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tawabocah.constants.PagingAndSortingConstants;
import com.tawabocah.hibernate.domain.PostWorksheet;
import com.tawabocah.service.PostWorksheetService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(path = "/api/v1")
public class PostWorksheetRestController {

	@Autowired
	private PostWorksheetService postWorksheetService;

	@ApiOperation(nickname = "Find published posts", value = "Find published post worksheets", notes = "Find published post worksheets from beginning of time until now.")
	@RequestMapping(path = "/post/worksheet/published/list", method = RequestMethod.GET)
	public List<PostWorksheet> findPublishedPageable(
			@ApiParam(value = "Number of items to be displayed on one page", allowableValues = "10,20,40", defaultValue = StringUtils.EMPTY
					+ PagingAndSortingConstants.INITIAL_PAGE_SIZE) @RequestParam(value = "ps", required = false) Integer pageSize,
			@ApiParam(value = "Page number requested (pagination based on page size and total items", allowableValues = "1,2,3,4,5", defaultValue = "1") @RequestParam(value = "p", required = false) Integer page,
			@ApiParam(value = "Sort by", allowableValues = "dateNewest,dateOldest,titleAZ,titleZA,random", defaultValue = "dateNewest") @RequestParam(value = "sb", required = false) String sortBy) {
		Date publishedDateTo = Calendar.getInstance().getTime();
		String evalSortBy = StringUtils.isEmpty(sortBy) ? PagingAndSortingConstants.INITIAL_SORT_BY : sortBy;
		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize == null ? PagingAndSortingConstants.INITIAL_PAGE_SIZE : pageSize;
		boolean validPageSize = false;
		for (int allowedPageSize : PagingAndSortingConstants.PAGE_SIZES) {
			if (evalPageSize == allowedPageSize) {
				validPageSize = true;
				break;
			}
		}
		if (!validPageSize) {
			evalPageSize = PagingAndSortingConstants.INITIAL_PAGE_SIZE;
		}

		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page == null || page < 1) ? PagingAndSortingConstants.INITIAL_PAGE : page;

		return postWorksheetService.findPublishedPosts(evalPage, evalPageSize, evalSortBy, publishedDateTo);
	}

	@RequestMapping(path = "/post/worksheet/published/count", method = RequestMethod.GET)
	public long findPublishedPageableCount() {
		Date publishedDateTo = Calendar.getInstance().getTime();
		return postWorksheetService.count(null, publishedDateTo, null);
	}

	@RequestMapping(path = "/post/worksheet/{id}", method = RequestMethod.GET)
	public PostWorksheet getbyId(@PathVariable Long id) {
		return postWorksheetService.findOne(id);
	}

	@RequestMapping(path = "/post/worksheet/save", method = RequestMethod.POST)
	public long save(@RequestBody PostWorksheet postWorksheet) {
		Date now = Calendar.getInstance().getTime();

		postWorksheet.setCreationDate(now);
		postWorksheet.setLastUpdateDate(now);
		postWorksheet.setPublishedDate(now);

		PostWorksheet savedPost = postWorksheetService.save(postWorksheet);

		return savedPost == null ? 0 : savedPost.getId();
	}

}
