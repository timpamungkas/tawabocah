package com.tawabocah.hibernate.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Used to generate article post from sitegen. {@link PostArticle} = awan.
 * Although this class extends {@link PostItem}, it does not use Hibernate
 * inheritance strategy, due to Id using different sequence. Sequence required
 * on finding prev/next href, so sharing same sequence on {@link PostItem} will
 * cause ID not sequential and not effective tag on post
 * 
 * @author Timotius Pamungkas
 *
 */
@Entity
@Table(name = "post_articles")
@Data
public class PostArticle implements Comparable<PostArticle>, PostItem {
	@Column(name = "created_by")
	@JsonIgnore
	private int createdBy;

	@Column(name = "creation_date", nullable = false)
	@JsonIgnore
	private Date creationDate;

	@Lob
	@Column(name = "html_additional_content")
	private String htmlAdditionalContent;

	/**
	 * Overview HTML Content (top portion content)
	 */
	@Lob
	@Column(name = "html_main_content")
	private String htmlMainContent;

	@Id
	@Column(name = "article_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "post_articles_s")
	@SequenceGenerator(name = "post_articles_s", sequenceName = "post_articles_s", allocationSize = 1, initialValue = 1)
	private long id;

	/**
	 * Path to video files (if video). Youtube video pixels is fixed (e.g.
	 * 500x361, ..), while FB video might differ even if it has same width (e.g.
	 * 500x290, 500x400). Better save direct embedded iframe rather than trying
	 * to generate it.
	 */
	@Lob
	@Column(name = "iframe_video_code")
	private String iframeVideoCode;

	@Column(name = "last_update_date", nullable = false)
	@JsonIgnore
	private Date lastUpdateDate;

	@Column(name = "last_updated_by")
	@JsonIgnore
	private int lastUpdatedBy;

	/**
	 * Meta description
	 */
	@Column(name = "meta_description", length = 255)
	private String metaDescription;

	/**
	 * Tags & meta keywords, as comma separated
	 */
	@Column(name = "meta_keywords", length = 255)
	private String metaKeywords;

	/**
	 * Next post id to be used on thymeleaf. Must be initialized with 0.
	 */
	@Transient
	private long nextId = 0;

	@Column(name = "original_source", length = 255)
	@JsonIgnore
	private String originalSource;

	/**
	 * Previous post id to be used on thymeleaf. Must be initialized with 0.
	 */
	@Transient
	private long prevId = 0;

	@Column(name = "published_date", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, dd MMM yyyy HH:mm:ss zzz")
	private Date publishedDate;

	@Column(name = "thumbnail_alt", length = 255)
	private String thumbnailAlt;

	/**
	 * Path to thumbnail
	 */
	@Column(name = "thumbnail_path", length = 255)
	private String thumbnailPath;

	/**
	 * Human-readable title
	 */
	@Column(name = "title", length = 80, nullable = false)
	private String title;

	@Column(name = "type", length = 30, nullable = false)
	private String type = "artikel";

	@Override
	public int compareTo(PostArticle o) {
		int compare = this.getLastUpdateDate().compareTo(o.getLastUpdateDate());
		return compare == 0 ? this.getCreationDate().compareTo(o.getCreationDate()) : compare;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostArticle other = (PostArticle) obj;
		if (id != other.id)
			return false;
		if (originalSource == null) {
			if (other.originalSource != null)
				return false;
		} else if (!originalSource.equals(other.originalSource))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((originalSource == null) ? 0 : originalSource.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	public void setIframeVideoCode(String iframeVideoCode) {
		this.iframeVideoCode = iframeVideoCode;

		if (!StringUtils.isEmpty(iframeVideoCode)) {
			this.type = "video";
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostArticle [id=").append(id).append(", originalSource=").append(originalSource)
				.append(", title=").append(title).append("]");
		return builder.toString();
	}

}
