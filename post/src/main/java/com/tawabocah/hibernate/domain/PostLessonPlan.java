package com.tawabocah.hibernate.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tawabocah.pdf.PDFToDropboxContainer;

import lombok.Data;

/**
 * Used to generate lesson plan post from sitegen. {@link PostLessonPlan} =
 * pohon. Although this class extends {@link PostItem}, it does not use
 * Hibernate inheritance strategy, due to Id using different sequence. Sequence
 * required on finding prev/next href, so sharing same sequence on
 * {@link PostItem} will cause ID not sequential and not effective * tag on post
 * 
 * @author Timotius Pamungkas
 *
 */
@Entity
@Table(name = "post_lesson_plans")
@Data
public class PostLessonPlan implements Comparable<PostLessonPlan>, PostItem {

	/**
	 * Path to PDF asset
	 */
	@Column(name = "asset_path", length = 255, nullable = false)
	private String assetPath;
	
	@Column(name = "created_by")
	@JsonIgnore
	private int createdBy;
	
	@Column(name = "creation_date", nullable = false)
	@JsonIgnore
	private Date creationDate;
	
	@Column(name = "estimated_time", length = 50)
	private String estimatedTime;

	/**
	 * PAUD / TK / 1 SD, dll
	 */
	@Column(name = "grade", nullable = false)
	private String grade;
	
	/**
	 * Additional HTML Content
	 */
	@Lob
	@Column(name = "html_additional_content")
	private String htmlAdditionalContent;
	
	@Lob
	@Column(name = "html_extension_content")
	private String htmlExtensionContent;
	
	@Lob
	@Column(name = "html_lesson_content", nullable = false)
	private String htmlLessonContent;
	
	@Lob
	@Column(name = "html_overview_content")
	private String htmlOverviewContent;
	
	@Lob
	@Column(name = "html_prep_material_content")
	private String htmlPreparationMaterialContent;
	
	@Lob
	@Column(name = "html_prep_step_content")
	private String htmlPreparationStepContent;
	
	@Lob
	@Column(name = "html_review_content")
	private String htmlReviewContent;
	
	@Id	
	@Column(name = "lesson_plan_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "post_lesson_plans_s")
	@SequenceGenerator(name = "post_lesson_plans_s", sequenceName = "post_lesson_plans_s", allocationSize = 1, initialValue = 1)
	private long id;
	
	@Column(name = "image_alt", length = 150)
	private String imageAlt;
	
	/**
	 * Path to large Image (thumbnail may partial image only)
	 */
	@Column(name = "image_path", length = 255)
	private String imagePath;
	
	@Column(name = "last_update_date", nullable = false)
	@JsonIgnore
	private Date lastUpdateDate;
	
	@Column(name = "last_updated_by")
	@JsonIgnore
	private int lastUpdatedBy;
	
	/**
	 * Meta description
	 */
	@Column(name = "meta_description", length = 255)
	private String metaDescription;
	
	/**
	 * Tags & meta keywords, as comma separated
	 */
	@Column(name = "meta_keywords", length = 255)
	private String metaKeywords;
	
	/**
	 * Next post id to be used on thymeleaf. Must be initialized with 0.
	 */
	@Transient
	private long nextId = 0;
	
	@Column(name = "original_source", length = 255)
	private String originalSource;
	
	/**
	 * Previous post id to be used on thymeleaf. Must be initialized with 0.
	 */
	@Transient
	private long prevId = 0;

	@Column(name = "published_date", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, dd MMM yyyy HH:mm:ss zzz")
	private Date publishedDate;
	
	/**
	 * Path to thumbnail
	 */
	@Column(name = "thumbnail_path", length = 255)
	private String thumbnailPath;
	
	/**
	 * Human-readable title
	 */
	@Column(name = "title", length = 80, nullable = false)
	private String title;

	@Override
	public int compareTo(PostLessonPlan o) {
		int compare = this.getLastUpdateDate().compareTo(o.getLastUpdateDate());
		return compare == 0 ? this.getCreationDate().compareTo(o.getCreationDate()) : compare;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostLessonPlan other = (PostLessonPlan) obj;
		if (assetPath == null) {
			if (other.assetPath != null)
				return false;
		} else if (!assetPath.equals(other.assetPath))
			return false;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/**
	 * Duplicate implementation on {@link PostWorksheet}, {@link PostActivity},
	 * and {@link PostLessonPlan}
	 * 
	 * @param pDFToDropboxContainer
	 * @param overrideValueIfExists
	 */
	public void fetchFromPdfDropbox(PDFToDropboxContainer pDFToDropboxContainer, boolean overrideValueIfExists) {
		if (StringUtils.isEmpty(this.getAssetPath())
				|| (!StringUtils.isEmpty(this.getAssetPath()) && overrideValueIfExists)) {
			this.setAssetPath(pDFToDropboxContainer.getAssetDbxSharedLinkUrl());
		}

		if (StringUtils.isEmpty(this.getImagePath())
				|| (!StringUtils.isEmpty(this.getImagePath()) && overrideValueIfExists)) {
			if (!pDFToDropboxContainer.getImageMap().isEmpty()) {
				String imageDbxSharedLinkUrl = pDFToDropboxContainer.getImageMap()
						.get(pDFToDropboxContainer.getImageMap().keySet().iterator().next());
				this.setImagePath(imageDbxSharedLinkUrl);
			}
		}

		if (StringUtils.isEmpty(this.getThumbnailPath())
				|| (!StringUtils.isEmpty(this.getThumbnailPath()) && overrideValueIfExists)) {
			if (!pDFToDropboxContainer.getThumbnailMap().isEmpty()) {
				String imageDbxSharedLinkUrl = pDFToDropboxContainer.getThumbnailMap()
						.get(pDFToDropboxContainer.getThumbnailMap().keySet().iterator().next());
				this.setImagePath(imageDbxSharedLinkUrl);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetPath == null) ? 0 : assetPath.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostLessonPlan [assetPath=").append(assetPath).append(", id=").append(id).append(", title=")
				.append(title).append("]");
		return builder.toString();
	}

}
