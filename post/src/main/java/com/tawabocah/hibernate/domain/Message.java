package com.tawabocah.hibernate.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "sys_contact_us_messages")
@Data
public class Message implements Comparable<Message> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long id;

	@Column(name = "creation_date", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date creationDate;

	@Column(name = "created_by")
	@JsonIgnore
	private int createdBy;

	@Column(name = "email", length = 50, nullable = false)
	private String email;

	@Column(name = "content", length = 1000)
	private String content;

	@Column(name = "name", length = 30)
	private String name;

	@Override
	public int compareTo(Message m) {
		return this.getCreationDate().compareTo(m.getCreationDate());
	}

}
