package com.tawabocah.hibernate.dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.domain.PostArticle;
import com.tawabocah.spring.repository.PostArticleRepository;

@Repository
@Scope("prototype")
public class PostArticleDAO {
	@Autowired
	private PostArticleRepository postArticleRepository;
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return postArticleRepository.count();
	}

	@Transactional
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) throws SQLException {
		long result = 0l;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			c = session.createCriteria(PostArticle.class).setProjection(Projections.count("id"));

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			result = (long) c.uniqueResult();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error count activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}

		return result;
	}

	@Transactional
	public void delete(Iterable<PostArticle> postArticles) {
		postArticleRepository.delete(postArticles);
	}

	@Transactional
	public void delete(Long id) {
		postArticleRepository.delete(id);
	}

	@Transactional
	public void delete(PostArticle postArticle) {
		postArticleRepository.delete(postArticle);
	}

	@Transactional
	public boolean exists(Long id) {
		return postArticleRepository.exists(id);
	}

	@Transactional
	public Iterable<PostArticle> findAll() {
		return postArticleRepository.findAll();
	}

	/**
	 * Find specified article. Avoid fetching html content if necessary (e.g. for index/inquiry page). Result from
	 * such method can be used to populate thumbnails.
	 * 
	 * @param fetchHtmlContent
	 *            is true will fetch html content to {@link PostArticle}.
	 *            Proceed with cautions, fetching html means fetching full html
	 *            source (CLOB).
	 * @param fetchHtmlContent
	 *            if true will fetch clob for html content. Use false if such
	 *            content not required to be rendered
	 * @param publishedDateFrom
	 * @param publishedDateTo
	 * @param tags collection of tag
	 * @param startIndex hibernate start index, zero based. If < 0 will use 0 instead
	 * @param limit number of items retrieved. If 0 will get all items
	 * @param random if true will get random element (NOTE using mysql specific query)
	 * @param orders Hibernate order, results will be ordered accoridng sequences of this varargs
	 * @return list of post item
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<PostArticle> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int numItems, boolean random, Order... orders)
			throws SQLException {
		List<PostArticle> result = null;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			if (fetchHtmlContent) {
				c = session.createCriteria(PostArticle.class);
			} else {
				c = session.createCriteria(PostArticle.class)
						.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
								.add(Projections.property("metaKeywords"), "metaKeywords")
								.add(Projections.property("thumbnailPath"), "thumbnailPath")
								.add(Projections.property("thumbnailAlt"), "thumbnailAlt")
								.add(Projections.property("title"), "title")
								.add(Projections.property("publishedDate"), "publishedDate")
								.add(Projections.property("type"), "type"))
						.setResultTransformer(Transformers.aliasToBean(PostArticle.class));
			}

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			if (random) {
				// MYSQL/Hibernate Specific!!!
				// http://stackoverflow.com/questions/2810693/hibernate-criteria-api-get-n-random-rows
				// only add 1 order, because if add other "order", will create double "order by" on query
				c.add(Restrictions.sqlRestriction("1=1 order by rand()"));
			} else {
				if (orders.length == 0) {
					c.addOrder(Order.desc("publishedDate"));
				} else {
					for (Order order : orders) {
						c.addOrder(order);
					}
				}
			}

			if (startIndex < 0) {
				startIndex = 0;
			}

			if (numItems > 0) {
				result = c.setFirstResult(startIndex).setMaxResults(numItems).list();
			} else {
				result = c.list();
			}

			return result == null ? Lists.newArrayList() : result;
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error find activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public Iterable<PostArticle> findAll(Iterable<Long> ids) {
		return postArticleRepository.findAll(ids);
	}

	@Transactional
	public Page<PostArticle> findAll(Pageable pageable) {
		return postArticleRepository.findAll(pageable);
	}

	@Transactional
	public PostArticle findNextArticle(PostArticle basedOnArticle) throws SQLException {
		long guessedNextId = basedOnArticle.getId() + 1;
		PostArticle result = null;
		// avoid endless loop
		long maxBoundId = 0;

		try (Session s = sessionFactory.openSession()) {
			Criteria c = s.createCriteria(PostArticle.class);
			c.setProjection(Projections.max("id"));
			maxBoundId = (long) c.uniqueResult();

			while (result == null && guessedNextId <= maxBoundId) {
				result = findOne(guessedNextId);
				guessedNextId++;
			}
		}

		return result;
	}

	@Transactional
	public PostArticle findOne(Long id) {
		return postArticleRepository.findOne(id);
	}

	@Transactional
	public PostArticle findPrevArticle(PostArticle basedOnArticle) throws SQLException {
		long guessedPrevId = basedOnArticle.getId() - 1;
		PostArticle result = null;
		// avoid endless loop
		long minBoundId = 0;

		while (result == null && guessedPrevId >= minBoundId) {
			try (Session s = sessionFactory.openSession()) {
				Criteria c = s.createCriteria(PostArticle.class);
				c.setProjection(Projections.min("id"));
				minBoundId = (long) c.uniqueResult();

				result = findOne(guessedPrevId);
				guessedPrevId--;
			}
		}

		return result;
	}

	@Transactional
	public Iterable<PostArticle> save(Iterable<PostArticle> postArticles) {
		return postArticleRepository.save(postArticles);
	}

	@Transactional
	public PostArticle save(PostArticle postArticle) {
		return postArticleRepository.save(postArticle);
	}

	@Transactional
	public void saveOrUpdate(PostArticle postArticle) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(postArticle);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error saveOrUpdate article: ").append(postArticle.getTitle())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public void update(PostArticle postArticle) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.update(postArticle);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error update article: ").append(postArticle.getTitle())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

}
