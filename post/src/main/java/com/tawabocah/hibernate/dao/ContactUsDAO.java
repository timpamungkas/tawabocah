package com.tawabocah.hibernate.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.domain.Message;
import com.tawabocah.spring.repository.ContactUsRepository;

@Repository
@Scope("prototype")
public class ContactUsDAO {

	@Autowired
	private ContactUsRepository contactUsRepository;
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return contactUsRepository.count();
	}

	@Transactional
	public void delete(Iterable<Message> messages) {
		contactUsRepository.delete(messages);
	}

	@Transactional
	public void delete(Long id) {
		contactUsRepository.delete(id);
	}

	@Transactional
	public void delete(Message message) {
		contactUsRepository.delete(message);
	}

	@Transactional
	public boolean exists(Long id) {
		return contactUsRepository.exists(id);
	}

	@Transactional
	public Iterable<Message> findAll() {
		return contactUsRepository.findAll();
	}

	@Transactional
	public Iterable<Message> findAll(Iterable<Long> ids) {
		return contactUsRepository.findAll(ids);
	}

	@Transactional
	public Message findOne(Long id) {
		return contactUsRepository.findOne(id);
	}

	@Transactional
	public Iterable<Message> save(Iterable<Message> messages) {
		return contactUsRepository.save(messages);
	}

	@Transactional
	public Message save(Message message) {
		return contactUsRepository.save(message);
	}

	@SuppressWarnings("unchecked")
	public List<Message> findAll(Order order) {
		List<Message> result = Lists.newArrayList();

		try (Session session = sessionFactory.openSession()) {
			Criteria c = session.createCriteria(Message.class);

			c.addOrder(order);

			result = c.list();

			result.forEach(m -> System.out.println(m.getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

}
