package com.tawabocah.hibernate.dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.domain.PostActivity;
import com.tawabocah.spring.repository.PostActivityRepository;

@Repository
@Scope("prototype")
public class PostActivityDAO {

	@Autowired
	private PostActivityRepository postActivityRepository;
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return postActivityRepository.count();
	}

	@Transactional
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) throws SQLException {
		long result = 0l;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			c = session.createCriteria(PostActivity.class).setProjection(Projections.count("id"));

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			result = (long) c.uniqueResult();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error count activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}

		return result;
	}

	@Transactional
	public void delete(Iterable<PostActivity> postActivities) {
		postActivityRepository.delete(postActivities);
	}

	@Transactional
	public void delete(Long id) {
		postActivityRepository.delete(id);
	}

	@Transactional
	public void delete(PostActivity postActivity) {
		postActivityRepository.delete(postActivity);
	}

	@Transactional
	public boolean exists(Long id) {
		return postActivityRepository.exists(id);
	}

	@Transactional
	public Iterable<PostActivity> findAll() {
		return postActivityRepository.findAll();
	}

	/**
	 * Find specified activity. Avoid fetching html content if necessary (e.g.
	 * for index/inquiry page). Result from such method can be used to populate
	 * thumbnails.
	 * 
	 * @param fetchHtmlContent
	 *            is true will fetch html content to {@link PostActivity}.
	 *            Proceed with cautions, fetching html means fetching full html
	 *            source (CLOB).
	 * @param fetchHtmlContent
	 *            if true will fetch clob for html content. Use false if such
	 *            content not required to be rendered
	 * @param publishedDateFrom
	 * @param publishedDateTo
	 * @param tags
	 *            collection of tag
	 * @param startIndex
	 *            hibernate start index, zero based. If < 0 will use 0 instead
	 * @param limit
	 *            number of items retrieved. If 0 will get all items
	 * @param random
	 *            if true will get random element (NOTE using mysql specific
	 *            query)
	 * @param orders
	 *            Hibernate order, results will be ordered accoridng sequences
	 *            of this varargs
	 * @return list of post item
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<PostActivity> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) throws SQLException {
		List<PostActivity> result = null;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			if (fetchHtmlContent) {
				c = session.createCriteria(PostActivity.class);
			} else {
				c = session.createCriteria(PostActivity.class)
						.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
								.add(Projections.property("assetPath"), "assetPath")
								.add(Projections.property("grade"), "grade")
								.add(Projections.property("imagePath"), "imagePath")
								.add(Projections.property("metaKeywords"), "metaKeywords")
								.add(Projections.property("imageAlt"), "imageAlt")
								.add(Projections.property("thumbnailPath"), "thumbnailPath")
								.add(Projections.property("title"), "title")
								.add(Projections.property("publishedDate"), "publishedDate"))
						.setResultTransformer(Transformers.aliasToBean(PostActivity.class));
			}

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			if (random) {
				// MYSQL/Hibernate Specific!!!
				// http://stackoverflow.com/questions/2810693/hibernate-criteria-api-get-n-random-rows
				// only add 1 order, because if add other "order", will create
				// double "order by" on query
				c.add(Restrictions.sqlRestriction("1=1 order by rand()"));
			} else {
				if (orders.length == 0) {
					c.addOrder(Order.desc("publishedDate"));
				} else {
					for (Order order : orders) {
						c.addOrder(order);
					}
				}
			}

			if (startIndex < 0) {
				startIndex = 0;
			}

			if (limit > 0) {
				result = c.setFirstResult(startIndex).setMaxResults(limit).list();
			} else {
				result = c.list();
			}

			return result == null ? Lists.newArrayList() : result;
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error find activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public Iterable<PostActivity> findAll(Iterable<Long> ids) {
		return postActivityRepository.findAll(ids);
	}

	@Transactional
	public Page<PostActivity> findAll(Pageable pageable) {
		return postActivityRepository.findAll(pageable);
	}

	@Transactional
	public PostActivity findNextActivity(PostActivity basedOnActivity) throws SQLException {
		long guessedNextId = basedOnActivity.getId() + 1;
		PostActivity result = null;
		// avoid endless loop
		long maxBoundId = 0;

		try (Session s = sessionFactory.openSession()) {
			Criteria c = s.createCriteria(PostActivity.class);
			c.setProjection(Projections.max("id"));
			maxBoundId = (long) c.uniqueResult();

			while (result == null && guessedNextId <= maxBoundId) {
				result = findOne(guessedNextId);
				guessedNextId++;
			}
		}

		return result;
	}

	@Transactional
	public PostActivity findOne(Long id) {
		return postActivityRepository.findOne(id);
	}

	@Transactional
	public PostActivity findPrevActivity(PostActivity basedOnActivity) throws SQLException {
		long guessedPrevId = basedOnActivity.getId() - 1;
		PostActivity result = null;
		// avoid endless loop
		long minBoundId = 0;

		while (result == null && guessedPrevId >= minBoundId) {
			try (Session s = sessionFactory.openSession()) {
				Criteria c = s.createCriteria(PostActivity.class);
				c.setProjection(Projections.min("id"));
				minBoundId = (long) c.uniqueResult();

				result = findOne(guessedPrevId);
				guessedPrevId--;
			}
		}

		return result;
	}

	@Transactional
	public Iterable<PostActivity> save(Iterable<PostActivity> postActivities) {
		return postActivityRepository.save(postActivities);
	}

	@Transactional
	public PostActivity save(PostActivity postActivity) {
		return postActivityRepository.save(postActivity);
	}

	@Transactional
	public void saveOrUpdate(PostActivity postActivity) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(postActivity);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error saveOrUpdate activity: ").append(postActivity.getAssetPath())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public void update(PostActivity postActivity) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.update(postActivity);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error update activity: ").append(postActivity.getAssetPath())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}
}
