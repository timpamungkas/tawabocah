package com.tawabocah.hibernate.dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.domain.PostLessonPlan;
import com.tawabocah.spring.repository.PostLessonPlanRepository;

@Repository
@Scope("prototype")
public class PostLessonPlanDAO {
	@Autowired
	private PostLessonPlanRepository postLessonPlanRepository;
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return postLessonPlanRepository.count();
	}

	@Transactional
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) throws SQLException {
		long result = 0l;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			c = session.createCriteria(PostLessonPlan.class).setProjection(Projections.count("id"));

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			result = (long) c.uniqueResult();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error count activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}

		return result;
	}

	@Transactional
	public void delete(Iterable<PostLessonPlan> postLessonPlans) {
		postLessonPlanRepository.delete(postLessonPlans);
	}

	@Transactional
	public void delete(Long id) {
		postLessonPlanRepository.delete(id);
	}

	@Transactional
	public void delete(PostLessonPlan postLessonPlan) {
		postLessonPlanRepository.delete(postLessonPlan);
	}

	@Transactional
	public boolean exists(Long id) {
		return postLessonPlanRepository.exists(id);
	}

	@Transactional
	public Iterable<PostLessonPlan> findAll() {
		return postLessonPlanRepository.findAll();
	}

	/**
	 * Find specified lesson plan. Avoid fetching html content if necessary (e.g.
	 * for index/inquiry page). Result from such method can be used to populate
	 * thumbnails.
	 * 
	 * @param fetchHtmlContent
	 *            if true will fetch clob for html content. Use false if such
	 *            content not required to be rendered
	 * @param publishedDateFrom
	 * @param publishedDateTo
	 * @param tags collection of tag
	 * @param startIndex hibernate start index, zero based. If < 0 will use 0 instead
	 * @param limit number of items retrieved. If 0 will get all items
	 * @param random if true will get random element (NOTE using mysql specific query)
	 * @param orders Hibernate order, results will be ordered accoridng sequences of this varargs
	 * @return list of post item
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<PostLessonPlan> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) throws SQLException {
		List<PostLessonPlan> result = null;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			if (fetchHtmlContent) {
				c = session.createCriteria(PostLessonPlan.class);
			} else {
				c = session.createCriteria(PostLessonPlan.class)
						.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
								.add(Projections.property("assetPath"), "assetPath")
								.add(Projections.property("grade"), "grade")
								.add(Projections.property("imagePath"), "imagePath")
								.add(Projections.property("metaKeywords"), "metaKeywords")
								.add(Projections.property("imageAlt"), "imageAlt")
								.add(Projections.property("thumbnailPath"), "thumbnailPath")
								.add(Projections.property("title"), "title")
								.add(Projections.property("publishedDate"), "publishedDate"))
						.setResultTransformer(Transformers.aliasToBean(PostLessonPlan.class));
			}

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			if (random) {
				// MYSQL/Hibernate Specific!!!
				// http://stackoverflow.com/questions/2810693/hibernate-criteria-api-get-n-random-rows
				// only add 1 order, because if add other "order", will create
				// double "order by" on query
				c.add(Restrictions.sqlRestriction("1=1 order by rand()"));
			} else {
				if (orders.length == 0) {
					c.addOrder(Order.desc("publishedDate"));
				} else {
					for (Order order : orders) {
						c.addOrder(order);
					}
				}
			}

			if (startIndex < 0) {
				startIndex = 0;
			}

			if (limit > 0) {
				result = c.setFirstResult(startIndex).setMaxResults(limit).list();
			} else {
				result = c.list();
			}

			return result == null ? Lists.newArrayList() : result;
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error find lesson plans : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public Iterable<PostLessonPlan> findAll(Iterable<Long> ids) {
		return postLessonPlanRepository.findAll(ids);
	}

	@Transactional
	public Page<PostLessonPlan> findAll(Pageable pageable) {
		return postLessonPlanRepository.findAll(pageable);
	}

	@Transactional
	public PostLessonPlan findNextLessonPlan(PostLessonPlan basedOnLessonPlan) throws SQLException {
		long guessedNextId = basedOnLessonPlan.getId() + 1;
		PostLessonPlan result = null;
		// avoid endless loop
		long maxBoundId = 0;

		try (Session s = sessionFactory.openSession()) {
			Criteria c = s.createCriteria(PostLessonPlan.class);
			c.setProjection(Projections.max("id"));
			maxBoundId = (long) c.uniqueResult();

			while (result == null && guessedNextId <= maxBoundId) {
				result = findOne(guessedNextId);
				guessedNextId++;
			}
		}

		return result;
	}

	@Transactional
	public PostLessonPlan findOne(Long id) {
		return postLessonPlanRepository.findOne(id);
	}

	public PostLessonPlan findPrevLessonPlan(PostLessonPlan basedOnLessonPlan) throws SQLException {
		long guessedPrevId = basedOnLessonPlan.getId() - 1;
		PostLessonPlan result = null;
		// avoid endless loop
		long minBoundId = 0;

		while (result == null && guessedPrevId >= minBoundId) {
			try (Session s = sessionFactory.openSession()) {
				Criteria c = s.createCriteria(PostLessonPlan.class);
				c.setProjection(Projections.min("id"));
				minBoundId = (long) c.uniqueResult();

				result = findOne(guessedPrevId);
				guessedPrevId--;
			}
		}

		return result;
	}

	@Transactional
	public Iterable<PostLessonPlan> save(Iterable<PostLessonPlan> postLessonPlans) {
		return postLessonPlanRepository.save(postLessonPlans);
	}

	@Transactional
	public PostLessonPlan save(PostLessonPlan postLessonPlan) {
		return postLessonPlanRepository.save(postLessonPlan);
	}

	@Transactional
	public void saveOrUpdate(PostLessonPlan postLessonPlan) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(postLessonPlan);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error save lesson plan: ").append(postLessonPlan.getAssetPath())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public void update(PostLessonPlan postLessonPlan) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.update(postLessonPlan);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error update lesson plan: ").append(postLessonPlan.getAssetPath())
					.append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

}
