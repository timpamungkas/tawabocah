package com.tawabocah.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {
	@Bean
	public Docket postApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.regex("/api/.*")).build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		Contact contact = new Contact("Tawabocah", "http://www.tawabocah.com", "tawabocah@gmail.com");

		ApiInfo apiInfo = new ApiInfo("Tawabocah Post API",
				"Tawabocah post REST api for handle data about posts. Posts includes worksheets, articles, activities, & lesson plans",
				"Version 1.0", "", contact, "Apache 2.0 License", "Apache 2.0 License URL");

		return apiInfo;
	}
}
