package com.tawabocah.service;

import com.tawabocah.hibernate.domain.PostArticle;

public interface PostArticleService extends PostItemService<PostArticle> {

}
