package com.tawabocah.service.impl;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.dao.PostArticleDAO;
import com.tawabocah.hibernate.domain.PostArticle;
import com.tawabocah.service.PostArticleService;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class PostArticleServiceImpl implements PostArticleService {

	private static final Logger LOGGER = LogManager.getLogger(PostArticleServiceImpl.class);

	@Autowired
	private PostArticleDAO postArticleDAO;

	@Transactional
	@Override
	public long count() {
		return postArticleDAO.count();
	}

	@Override
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) {
		try {
			return postArticleDAO.count(publishedDateFrom, publishedDateTo, tags);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Transactional
	@Override
	public void delete(Iterable<PostArticle> postArticles) {
		postArticleDAO.delete(postArticles);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		postArticleDAO.delete(id);
	}

	@Transactional
	@Override
	public void delete(PostArticle postArticle) {
		postArticleDAO.delete(postArticle);
	}

	@Transactional
	@Override
	public boolean exists(Long id) {
		return postArticleDAO.exists(id);
	}

	@Transactional
	@Override
	public Iterable<PostArticle> findAll() {
		return postArticleDAO.findAll();
	}

	@Transactional
	@Override
	public List<PostArticle> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int numItems, boolean random, Order... orders) {
		List<PostArticle> allArticles;
		try {
			allArticles = postArticleDAO.findAll(fetchHtmlContent, publishedDateFrom, publishedDateTo, tags, startIndex,
					numItems, random, orders);
		} catch (SQLException e) {
			allArticles = Lists.newArrayList();
			log.error("Error findAll : " + e.getMessage());
		}

		return allArticles;
	}

	@Transactional
	@Override
	public Iterable<PostArticle> findAll(Iterable<Long> ids) {
		return postArticleDAO.findAll(ids);
	}

	@Transactional
	@Override
	public Page<PostArticle> findAll(Pageable pageable) {
		return postArticleDAO.findAll(pageable);
	}

	@Transactional
	@Override
	public PostArticle findOne(Long id) {
		PostArticle result = postArticleDAO.findOne(id);

		if (result != null) {
			// get prevId & nextId
			try {
				PostArticle prevArticle = postArticleDAO.findPrevArticle(result);
				PostArticle nextArticle = postArticleDAO.findNextArticle(result);

				result.setPrevId(prevArticle == null ? 0 : prevArticle.getId());
				result.setNextId(nextArticle == null ? 0 : nextArticle.getId());
			} catch (SQLException ex) {
				LOGGER.error("Error find prev / next post : " + ex.getMessage());
			}
		}

		return result;
	}

	@Override
	public List<PostArticle> findPublishedPosts(Integer pageNumber, Integer pageSize, String sortBy,
			Date publishedDateTo) {
		Order sortByParam = Order.desc("publishedDate");
		Order sortbyIdDesc = Order.desc("id");
		boolean random = false;

		switch (sortBy) {
		case "dateNewest":
			sortByParam = Order.desc("publishedDate");
			break;
		case "dateOldest":
			sortByParam = Order.asc("publishedDate");
			break;
		case "titleAZ":
			sortByParam = Order.asc("title");
			break;
		case "titleZA":
			sortByParam = Order.desc("title");
			break;
		case "random":
			random = true;
			break;
		}

		List<PostArticle> list = findAll(false, null, publishedDateTo, null, (pageNumber - 1) * pageSize, pageSize,
				random, sortByParam, sortbyIdDesc);

		return list;
	}

	@Transactional
	@Override
	public Iterable<PostArticle> save(Iterable<PostArticle> postArticles) {
		return postArticleDAO.save(postArticles);
	}

	@Transactional
	@Override
	public PostArticle save(PostArticle postArticle) {
		return postArticleDAO.save(postArticle);
	}

}
