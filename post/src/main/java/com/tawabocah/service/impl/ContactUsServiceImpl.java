package com.tawabocah.service.impl;

import java.util.Calendar;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tawabocah.hibernate.dao.ContactUsDAO;
import com.tawabocah.hibernate.domain.Message;
import com.tawabocah.service.ContactUsService;
import com.tawabocah.util.MailUtil;

@Service
public class ContactUsServiceImpl implements ContactUsService {

	@Autowired
	private ContactUsDAO contactUsDAO;

	@Autowired
	private MailUtil mailUtil;

	@Value("${freshdesk.mail}")
	public String freshdeskMail;

	@Override
	@Transactional
	public long count() {
		return contactUsDAO.count();
	}

	@Override
	@Transactional
	public void delete(Iterable<Message> Messages) {
		contactUsDAO.delete(Messages);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		contactUsDAO.delete(id);
	}

	@Override
	@Transactional
	public void delete(Message Message) {
		contactUsDAO.delete(Message);
	}

	@Override
	@Transactional
	public boolean exists(Long id) {
		return contactUsDAO.exists(id);
	}

	@Override
	@Transactional
	public Iterable<Message> findAll() {
		return contactUsDAO.findAll();
	}

	@Override
	@Transactional
	public Iterable<Message> findAll(Iterable<Long> ids) {
		return contactUsDAO.findAll(ids);
	}

	@Override
	@Transactional
	public Message findOne(Long id) {
		return contactUsDAO.findOne(id);
	}

	@Override
	@Transactional
	public Iterable<Message> save(Iterable<Message> messages) {
		return contactUsDAO.save(messages);
	}

	@Override
	@Transactional
	public Message save(Message message) {
		return contactUsDAO.save(message);
	}

	@Override
	public boolean saveAndSendMail(Message message) throws MessagingException {
		message.setCreationDate(Calendar.getInstance().getTime());
		
		Message savedMessage = save(message);

		if (savedMessage == null) {
			return false;
		}

		mailUtil.send(freshdeskMail, null, mailUtil.me, "[Contact Us] From : " + message.getName(),
				message.getContent() + "<br/><br/>reply to : " + message.getEmail(), true);

		return true;
	}

}
