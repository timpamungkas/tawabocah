package com.tawabocah.service.impl;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.dao.PostWorksheetDAO;
import com.tawabocah.hibernate.domain.PostWorksheet;
import com.tawabocah.service.PostWorksheetService;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class PostWorksheetServiceImpl implements PostWorksheetService {
	private static final Logger LOGGER = LogManager.getLogger(PostWorksheetServiceImpl.class);

	@Autowired
	private PostWorksheetDAO postWorksheetDAO;

	@Transactional
	@Override
	public long count() {
		return postWorksheetDAO.count();
	}

	@Override
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) {
		try {
			return postWorksheetDAO.count(publishedDateFrom, publishedDateTo, tags);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Transactional
	@Override
	public void delete(Iterable<PostWorksheet> postWorksheets) {
		postWorksheetDAO.delete(postWorksheets);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		postWorksheetDAO.delete(id);
	}

	@Transactional
	@Override
	public void delete(PostWorksheet postWorksheet) {
		postWorksheetDAO.delete(postWorksheet);
	}

	@Transactional
	@Override
	public boolean exists(Long id) {
		return postWorksheetDAO.exists(id);
	}

	@Transactional
	@Override
	public Iterable<PostWorksheet> findAll() {
		return postWorksheetDAO.findAll();
	}

	@Transactional
	@Override
	public List<PostWorksheet> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) {
		List<PostWorksheet> allWorksheets;
		try {
			allWorksheets = postWorksheetDAO.findAll(fetchHtmlContent, publishedDateFrom, publishedDateTo, tags,
					startIndex, limit, random, orders);
		} catch (SQLException e) {
			allWorksheets = Lists.newArrayList();
			log.error("Error findAll : " + e.getMessage());
		}

		return allWorksheets;
	}

	@Transactional
	@Override
	public Iterable<PostWorksheet> findAll(Iterable<Long> ids) {
		return postWorksheetDAO.findAll(ids);
	}

	@Transactional
	@Override
	public Page<PostWorksheet> findAll(Pageable pageable) {
		return postWorksheetDAO.findAll(pageable);
	}

	/**
	 * Find exact worksheet, along with meta keyword as set, and previous / next
	 * worksheet Id (if any). Default prevId and nextId is 0, which means this
	 * post is a single post.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	@Override
	public PostWorksheet findOne(Long id) {
		PostWorksheet result = postWorksheetDAO.findOne(id);

		// get prevId & nextId
		try {
			PostWorksheet prevWorksheet = postWorksheetDAO.findPrevWorksheet(result);
			PostWorksheet nextWorksheet = postWorksheetDAO.findNextWorksheet(result);

			result.setPrevId(prevWorksheet == null ? 0 : prevWorksheet.getId());
			result.setNextId(nextWorksheet == null ? 0 : nextWorksheet.getId());
		} catch (SQLException ex) {
			LOGGER.error("Error find prev / next post : " + ex.getMessage());
		}

		return result;
	}

	@Override
	public List<PostWorksheet> findPublishedPosts(Integer pageNumber, Integer pageSize, String sortBy,
			Date publishedDateTo) {
		Order sortByParam = Order.desc("publishedDate");
		Order sortbyIdDesc = Order.desc("id");
		boolean random = false;

		switch (sortBy) {
		case "dateNewest":
			sortByParam = Order.desc("publishedDate");
			break;
		case "dateOldest":
			sortByParam = Order.asc("publishedDate");
			break;
		case "titleAZ":
			sortByParam = Order.asc("title");
			break;
		case "titleZA":
			sortByParam = Order.desc("title");
			break;
		case "random":
			random = true;
			break;
		}

		List<PostWorksheet> list = findAll(false, null, publishedDateTo, null, (pageNumber - 1) * pageSize, pageSize,
				random, sortByParam, sortbyIdDesc);

		return list;
	}

	@Transactional
	@Override
	public Iterable<PostWorksheet> save(Iterable<PostWorksheet> postWorksheets) {
		return postWorksheetDAO.save(postWorksheets);
	}

	@Transactional
	@Override
	public PostWorksheet save(PostWorksheet postWorksheet) {
		return postWorksheetDAO.save(postWorksheet);
	}
}
