package com.tawabocah.service.impl;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.dao.PostActivityDAO;
import com.tawabocah.hibernate.domain.PostActivity;
import com.tawabocah.service.PostActivityService;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class PostActivityServiceImpl implements PostActivityService {
	private static final Logger LOGGER = LogManager.getLogger(PostActivityServiceImpl.class);

	@Autowired
	private PostActivityDAO postActivityDAO;

	@Transactional
	@Override
	public long count() {
		return postActivityDAO.count();
	}

	@Override
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) {
		try {
			return postActivityDAO.count(publishedDateFrom, publishedDateTo, tags);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Transactional
	@Override
	public void delete(Iterable<PostActivity> postActivities) {
		postActivityDAO.delete(postActivities);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		postActivityDAO.delete(id);
	}

	@Transactional
	@Override
	public void delete(PostActivity postActivity) {
		postActivityDAO.delete(postActivity);
	}

	@Transactional
	@Override
	public boolean exists(Long id) {
		return postActivityDAO.exists(id);
	}

	@Transactional
	@Override
	public Iterable<PostActivity> findAll() {
		return postActivityDAO.findAll();
	}

	@Transactional
	@Override
	public List<PostActivity> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) {
		List<PostActivity> allActivities;
		try {
			allActivities = postActivityDAO.findAll(fetchHtmlContent, publishedDateFrom, publishedDateTo, tags,
					startIndex, limit, random, orders);
		} catch (SQLException e) {
			allActivities = Lists.newArrayList();
			log.error("Error findAll : " + e.getMessage());
		}

		return allActivities;
	}

	@Transactional
	@Override
	public Iterable<PostActivity> findAll(Iterable<Long> ids) {
		return postActivityDAO.findAll(ids);
	}

	@Transactional
	@Override
	public Page<PostActivity> findAll(Pageable pageable) {
		return postActivityDAO.findAll(pageable);
	}

	/**
	 * Find exact worksheet, along with meta keyword as set, and previous / next
	 * worksheet Id (if any). Default prevId and nextId is 0, which means this
	 * post is a single post.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	@Override
	public PostActivity findOne(Long id) {
		PostActivity result = postActivityDAO.findOne(id);

		// get prevId & nextId
		try {
			PostActivity prevActivity = postActivityDAO.findPrevActivity(result);
			PostActivity nextActivity = postActivityDAO.findNextActivity(result);

			result.setPrevId(prevActivity == null ? 0 : prevActivity.getId());
			result.setNextId(nextActivity == null ? 0 : nextActivity.getId());
		} catch (SQLException ex) {
			LOGGER.error("Error find prev / next post : " + ex.getMessage());
		}

		return result;
	}

	@Override
	public List<PostActivity> findPublishedPosts(Integer pageNumber, Integer pageSize, String sortBy,
			Date publishedDateTo) {
		Order sortByParam = Order.desc("publishedDate");
		Order sortbyIdDesc = Order.desc("id");
		boolean random = false;

		switch (sortBy) {
		case "dateNewest":
			sortByParam = Order.desc("publishedDate");
			break;
		case "dateOldest":
			sortByParam = Order.asc("publishedDate");
			break;
		case "titleAZ":
			sortByParam = Order.asc("title");
			break;
		case "titleZA":
			sortByParam = Order.desc("title");
			break;
		case "random":
			random = true;
			break;
		}

		List<PostActivity> list = findAll(false, null, publishedDateTo, null, (pageNumber - 1) * pageSize, pageSize,
				random, sortByParam, sortbyIdDesc);

		return list;
	}

	@Transactional
	@Override
	public Iterable<PostActivity> save(Iterable<PostActivity> postActivities) {
		return postActivityDAO.save(postActivities);
	}

	@Transactional
	@Override
	public PostActivity save(PostActivity postActivity) {
		return postActivityDAO.save(postActivity);
	}

}
