package com.tawabocah.service.impl;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.dao.PostLessonPlanDAO;
import com.tawabocah.hibernate.domain.PostLessonPlan;
import com.tawabocah.service.PostLessonPlanService;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class PostLessonPlanServiceImpl implements PostLessonPlanService {
	private static final Logger LOGGER = LogManager.getLogger(PostLessonPlanServiceImpl.class);

	@Autowired
	private PostLessonPlanDAO postLessonPlanDAO;

	@Transactional
	@Override
	public long count() {
		return postLessonPlanDAO.count();
	}

	@Override
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) {
		try {
			return postLessonPlanDAO.count(publishedDateFrom, publishedDateTo, tags);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Transactional
	@Override
	public void delete(Iterable<PostLessonPlan> postLessonPlans) {
		postLessonPlanDAO.delete(postLessonPlans);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		postLessonPlanDAO.delete(id);
	}

	@Transactional
	@Override
	public void delete(PostLessonPlan postLessonPlan) {
		postLessonPlanDAO.delete(postLessonPlan);
	}

	@Transactional
	@Override
	public boolean exists(Long id) {
		return postLessonPlanDAO.exists(id);
	}

	@Transactional
	@Override
	public Iterable<PostLessonPlan> findAll() {
		return postLessonPlanDAO.findAll();
	}

	@Transactional
	@Override
	public List<PostLessonPlan> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) {
		List<PostLessonPlan> allLessonPlans;
		try {
			allLessonPlans = postLessonPlanDAO.findAll(fetchHtmlContent, publishedDateFrom, publishedDateTo, tags,
					startIndex, limit, random, orders);
		} catch (SQLException e) {
			allLessonPlans = Lists.newArrayList();
			log.error("Error findAll : " + e.getMessage());
		}

		return allLessonPlans;
	}

	@Transactional
	@Override
	public Iterable<PostLessonPlan> findAll(Iterable<Long> ids) {
		return postLessonPlanDAO.findAll(ids);
	}

	@Transactional
	@Override
	public Page<PostLessonPlan> findAll(Pageable pageable) {
		return postLessonPlanDAO.findAll(pageable);
	}

	/**
	 * Find exact lesson plan, along with meta keyword as set, and previous /
	 * next lesson plan Id (if any). Default prevId and nextId is 0, which means
	 * this post is a single post.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	@Override
	public PostLessonPlan findOne(Long id) {
		PostLessonPlan result = postLessonPlanDAO.findOne(id);

		// get prevId & nextId
		try {
			PostLessonPlan prevLessonPlan = postLessonPlanDAO.findPrevLessonPlan(result);
			PostLessonPlan nextLessonPlan = postLessonPlanDAO.findNextLessonPlan(result);

			result.setPrevId(prevLessonPlan == null ? 0 : prevLessonPlan.getId());
			result.setNextId(nextLessonPlan == null ? 0 : nextLessonPlan.getId());
		} catch (SQLException ex) {
			LOGGER.error("Error find prev / next post : " + ex.getMessage());
		}

		return result;
	}

	@Override
	public List<PostLessonPlan> findPublishedPosts(Integer pageNumber, Integer pageSize, String sortBy,
			Date publishedDateTo) {
		Order sortByParam = Order.desc("publishedDate");
		Order sortbyIdDesc = Order.desc("id");
		boolean random = false;

		switch (sortBy) {
		case "dateNewest":
			sortByParam = Order.desc("publishedDate");
			break;
		case "dateOldest":
			sortByParam = Order.asc("publishedDate");
			break;
		case "titleAZ":
			sortByParam = Order.asc("title");
			break;
		case "titleZA":
			sortByParam = Order.desc("title");
			break;
		case "random":
			random = true;
			break;
		}

		List<PostLessonPlan> list = findAll(false, null, publishedDateTo, null, (pageNumber - 1) * pageSize, pageSize,
				random, sortByParam, sortbyIdDesc);

		return list;
	}

	@Transactional
	@Override
	public Iterable<PostLessonPlan> save(Iterable<PostLessonPlan> postLessonPlans) {
		return postLessonPlanDAO.save(postLessonPlans);
	}

	@Transactional
	@Override
	public PostLessonPlan save(PostLessonPlan postLessonPlan) {
		return postLessonPlanDAO.save(postLessonPlan);
	}
}
