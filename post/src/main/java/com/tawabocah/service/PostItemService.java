package com.tawabocah.service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.tawabocah.constants.PagingAndSortingConstants;

public interface PostItemService<T> {

	long count();

	long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags);

	// data type-independent
	/**
	 * For Spring Pageable
	 * 
	 * @param page
	 * @param pageSize
	 * @param sortBy
	 * @return
	 */
	default PageRequest createPageRequest(Integer page, Integer pageSize, String sortBy) {
		PageRequest result = null;

		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize == null ? PagingAndSortingConstants.INITIAL_PAGE_SIZE : pageSize;
		boolean validPageSize = false;
		for (int allowedPageSize : PagingAndSortingConstants.PAGE_SIZES) {
			if (evalPageSize == allowedPageSize) {
				validPageSize = true;
				break;
			}
		}
		if (!validPageSize) {
			evalPageSize = PagingAndSortingConstants.INITIAL_PAGE_SIZE;
		}

		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page == null || page < 1) ? PagingAndSortingConstants.INITIAL_PAGE : page - 1;

		// evaluate sortBy (hard-coded) ?
		String evalSortBy = StringUtils.isEmpty(sortBy) ? PagingAndSortingConstants.INITIAL_SORT_BY : sortBy;
		Sort sort = null;
		switch (evalSortBy) {
		case "dateOldest":
			sort = new Sort(new Order(Direction.ASC, "publishedDate"));
			break;
		case "titleAZ":
			sort = new Sort(new Order(Direction.ASC, "title").ignoreCase(), new Order(Direction.DESC, "publishedDate"));
			break;
		case "titleZA":
			sort = new Sort(new Order(Direction.DESC, "title").ignoreCase(),
					new Order(Direction.DESC, "publishedDate"));
			break;
		case "random":
			break;
		case "dateNewest":
		default:
			// default by date newest
			sort = new Sort(new Order(Direction.DESC, "publishedDate"));
			break;
		}

		result = new PageRequest(evalPage, evalPageSize, sort);

		return result;
	}

	void delete(Iterable<T> ts);

	void delete(Long id);

	void delete(T t);

	boolean exists(Long id);

	Iterable<T> findAll();

	/**
	 * hibernate custom implementation
	 * 
	 * @param fetchHtmlContent
	 * @param publishedDateFrom
	 * @param publishedDateTo
	 * @param tags
	 * @param numItems
	 * @param shuffle
	 * @param orders
	 * @return
	 * @throws SQLException
	 */
	List<T> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo, Collection<String> tags,
			int startIndex, int numItems, boolean shuffle, org.hibernate.criterion.Order... orders);

	Iterable<T> findAll(Iterable<Long> ids);

	/**
	 * Finds a "page" of item
	 * 
	 * @param pageable
	 * @return {@link Page} instance
	 */
	Page<T> findAll(Pageable pageable);

	T findOne(Long id);

	List<T> findPublishedPosts(Integer pageNumber, Integer pageSize, String sortBy, Date publishedDateTo);

	Iterable<T> save(Iterable<T> ts);

	T save(T T);

}
