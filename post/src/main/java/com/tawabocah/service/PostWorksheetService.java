package com.tawabocah.service;

import com.tawabocah.hibernate.domain.PostWorksheet;

public interface PostWorksheetService extends PostItemService<PostWorksheet>  {

}
