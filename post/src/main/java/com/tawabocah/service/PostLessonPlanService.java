package com.tawabocah.service;

import com.tawabocah.hibernate.domain.PostLessonPlan;

public interface PostLessonPlanService extends PostItemService<PostLessonPlan>  {

}
