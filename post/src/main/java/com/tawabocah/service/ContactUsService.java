package com.tawabocah.service;

import javax.mail.MessagingException;

import com.tawabocah.hibernate.domain.Message;

public interface ContactUsService {

	public long count();

	public void delete(Iterable<Message> messages);

	public void delete(Long id);

	public void delete(Message message);

	public boolean exists(Long id);

	public Iterable<Message> findAll();

	public Iterable<Message> findAll(Iterable<Long> ids);

	public Message findOne(Long id);

	public Iterable<Message> save(Iterable<Message> messages);

	Message save(Message message);

	boolean saveAndSendMail(Message message) throws MessagingException;

}
