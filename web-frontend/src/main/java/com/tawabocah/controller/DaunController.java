package com.tawabocah.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tawabocah.constant.PagingAndSortingConstants;
import com.tawabocah.model.DaunPost;
import com.tawabocah.paging.Pager;
import com.tawabocah.service.DaunService;

@Controller
@RequestMapping(path = "/aktivitas-anak/daun")
public class DaunController {

	@Autowired
	private DaunService daunService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	private ModelAndView indexWithPaging(@RequestParam(value = "ps", required = false) Integer pageSize,
			@RequestParam(value = "p", required = false) Integer page,
			@RequestParam(value = "sb", required = false) String sortBy) {
		ModelAndView modelAndView = new ModelAndView("daun/index");

		String evalSortBy = StringUtils.isEmpty(sortBy) ? PagingAndSortingConstants.INITIAL_SORT_BY : sortBy;
		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize == null ? PagingAndSortingConstants.INITIAL_PAGE_SIZE : pageSize;
		boolean validPageSize = false;
		for (int allowedPageSize : PagingAndSortingConstants.PAGE_SIZES) {
			if (evalPageSize == allowedPageSize) {
				validPageSize = true;
				break;
			}
		}
		if (!validPageSize) {
			evalPageSize = PagingAndSortingConstants.INITIAL_PAGE_SIZE;
		}

		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page == null || page < 1) ? PagingAndSortingConstants.INITIAL_PAGE : page;

		Page<DaunPost> postActivities = daunService.findPublishedPageable(evalPage, evalPageSize, evalSortBy);

		Pager pager = new Pager(postActivities.getTotalPages(), postActivities.getNumber(),
				PagingAndSortingConstants.BUTTONS_TO_SHOW);

		modelAndView.addObject("postActivities", postActivities);
		modelAndView.addObject("selectedPageSize", evalPageSize);
		modelAndView.addObject("selectedSortBy", evalSortBy);
		modelAndView.addObject("pageSizes", PagingAndSortingConstants.PAGE_SIZES);
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("sortByOptions", PagingAndSortingConstants.SORT_BY_OPTIONS);

		return modelAndView;
	}

	@RequestMapping(path = "/baru", method = RequestMethod.GET)
	private String newPost() {
		return "daun/new";
	}
	
	@RequestMapping(path = "/baru", method = RequestMethod.POST)
	public String newPostSaveAjax(@RequestBody DaunPost daunPost) {
		daunService.save(daunPost);

		return "daun/new";
	}

	@RequestMapping(path = "/post", method = RequestMethod.GET)
	private String post(@RequestParam(value = "id", defaultValue = "1") String idString, Model model) {
		long id = 1;

		try {
			id = Long.parseLong(idString);
		} catch (Exception e) {
			id = 1;
		}

		DaunPost postActivity = daunService.getById(id);

		model.addAttribute("postActivity", postActivity);

		return "daun/post";
	}

}
