package com.tawabocah.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StaticPageController {

	@RequestMapping(path = "/coming_soon")
	private String comingSoon() {
		return "coming_soon";
	}

	@RequestMapping(path = "/")
	private String index() {
		return "index";
	}

	@RequestMapping(path = "/login", method = RequestMethod.GET)
	private String login() {
		return "login";
	}

	@RequestMapping(path="/matahari/syarat-ketentuan")
	private String licenseAgreement() {
		return "matahari/license-agreement";
	}
}