package com.tawabocah.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tawabocah.model.Message;
import com.tawabocah.service.ContactUsService;

@Controller
public class AboutUsController {

	@Autowired
	private ContactUsService contactUsService;

	@RequestMapping(path = "/tentang_kami", method = RequestMethod.GET)
	private String aboutUs() {
		return "about_us/about_us";
	}

	@RequestMapping(value = "/hubungi_kami", method = RequestMethod.GET)
	private String contactUs(@ModelAttribute Message message) {
		return "about_us/contact_us";
	}

	@RequestMapping(value = "/hubungi_kami", method = RequestMethod.POST)
	private String contactUsPost(@ModelAttribute Message message, Model model) {
		boolean isSaved = contactUsService.saveAndSendMail(message);

		model.addAttribute("isSaved", isSaved);

		return "about_us/contact_us";
	}

}
