package com.tawabocah.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tawabocah.constant.PagingAndSortingConstants;
import com.tawabocah.model.MatahariCatalogItem;
import com.tawabocah.paging.Pager;
import com.tawabocah.service.MatahariCatalogService;

@Controller
@RequestMapping(path = "/lembar-latihan/matahari")
public class MatahariController {

	@Autowired
	private MatahariCatalogService matahariCatalogService;

	@RequestMapping(path = "/beli", method = RequestMethod.GET)
	private String howToBuy() {
		return "matahari/how_to_buy";
	}

	@RequestMapping(path = "", method = RequestMethod.GET)
	private String index() {
		return "matahari/index";
	}

	@RequestMapping(path = "/cari", method = RequestMethod.GET)
	private String orderFind() {
		return "matahari/order_find";
	}

	@RequestMapping(path = "/baru", method = RequestMethod.GET)
	private String orderNew() {
		return "matahari/order_new";
	}

	@RequestMapping(path = "/katalog/detail", method = RequestMethod.GET)
	private String workbookCatalogItemDetail(@RequestParam(value = "id", defaultValue = "1") String idString,
			Model model) {
		long id = 1l;

		try {
			id = Integer.parseInt(idString);
		} catch (Exception e) {
			id = 1l;
		}

		MatahariCatalogItem workbookCatalogItem = matahariCatalogService.getById(id);

		model.addAttribute("workbookCatalogItem", workbookCatalogItem);

		return "matahari/workbook_catalog_detail";
	}

	@RequestMapping(value = "/katalog", method = RequestMethod.GET)
	private ModelAndView workbookCatalogWithPaging(@RequestParam(value = "ps", required = false) Integer pageSize,
			@RequestParam(value = "p", required = false) Integer page,
			@RequestParam(value = "sb", required = false) String sortBy,
			@RequestParam(value = "g", required = false) String grade) {
		ModelAndView modelAndView = new ModelAndView("matahari/workbook_catalog");

		String evalSortBy = StringUtils.isEmpty(sortBy) ? PagingAndSortingConstants.INITIAL_SORT_BY : sortBy;
		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize == null ? PagingAndSortingConstants.INITIAL_PAGE_SIZE : pageSize;
		boolean validPageSize = false;
		for (int allowedPageSize : PagingAndSortingConstants.PAGE_SIZES) {
			if (evalPageSize == allowedPageSize) {
				validPageSize = true;
				break;
			}
		}
		if (!validPageSize) {
			evalPageSize = PagingAndSortingConstants.INITIAL_PAGE_SIZE;
		}

		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page == null || page < 1) ? PagingAndSortingConstants.INITIAL_PAGE : page;
		String evalGrade = (grade == null ? StringUtils.EMPTY : grade);
		
		Page<MatahariCatalogItem> workbookCatalogItems = matahariCatalogService.findPublishedPageable(evalPage,
				evalPageSize, evalSortBy, evalGrade);

		Pager pager = new Pager(workbookCatalogItems.getTotalPages(), workbookCatalogItems.getNumber(),
				PagingAndSortingConstants.BUTTONS_TO_SHOW);

		modelAndView.addObject("workbookCatalogItems", workbookCatalogItems);
		modelAndView.addObject("selectedGrade", grade);
		modelAndView.addObject("selectedPageSize", evalPageSize);
		modelAndView.addObject("selectedSortBy", evalSortBy);
		modelAndView.addObject("pageSizes", PagingAndSortingConstants.PAGE_SIZES);
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("sortByOptions", PagingAndSortingConstants.SORT_BY_OPTIONS);

		return modelAndView;
	}

	@RequestMapping(path = "/buat", method = RequestMethod.GET)
	private String workbookGenerator() {
		return "matahari/workbook_generator";
	}

	@RequestMapping(path = "/buat/penjumlahan", method = RequestMethod.GET)
	private String workbookGeneratorAddition() {
		return "matahari/workbook_generator_addition";
	}

	@RequestMapping(path = "/buat/pembagian", method = RequestMethod.GET)
	private String workbookGeneratorDivision() {
		return "coming_soon";
	}

	@RequestMapping(path = "/buat/perkalian", method = RequestMethod.GET)
	private String workbookGeneratorMultiplication() {
		return "matahari/workbook_generator_multiplication";
	}

	@RequestMapping(path = "/buat/pengurangan", method = RequestMethod.GET)
	private String workbookGeneratorSubtraction() {
		return "matahari/workbook_generator_subtraction";
	}

}
