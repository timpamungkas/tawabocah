package com.tawabocah.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Charsets;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class DaunPost extends BasicAuthenticationPost {

	private String assetPath;
	private String grade;
	private String htmlActivityContent;
	private String htmlAdditionalContent;
	private String htmlOverviewContent;
	private String htmlRequirementContent;
	private long id;
	private String imageAlt;
	private String imagePath;
	private String metaDescription;
	private String metaKeywords;
	private long nextId = 0;
	private String originalSource;
	private long prevId = 0;
	private Date publishedDate;
	private String thumbnailPath;
	private String title;

	public boolean getContainsAdditionalContent() {
		return !StringUtils.isEmpty(htmlAdditionalContent);
	}

	public boolean getHasDownloadableAsset() {
		return !StringUtils.isEmpty(assetPath);
	}

	public boolean getHasNextActivity() {
		return getNextId() > 0;
	}

	public boolean getHasPrevActivity() {
		return getPrevId() > 0;
	}

	public boolean getHasThumbnail() {
		return !StringUtils.isEmpty(thumbnailPath);
	}

	public String getImagePathEncoded() {
		try {
			return URLEncoder.encode(imagePath, Charsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			return imagePath;
		}
	}

	public Set<String> getMetaKeywordSet() {
		Set<String> metaKeywordSet = Sets.newLinkedHashSet();
		for (String s : this.metaKeywords.split(",")) {
			metaKeywordSet.add(StringUtils.trim(s));
		}
		metaKeywordSet.add(grade);

		return metaKeywordSet;
	}

	public String getThumbnailPathEncoded() {
		try {
			return URLEncoder.encode(thumbnailPath, Charsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			return thumbnailPath;
		}
	}

	public String getTitleEncoded() {
		try {
			return URLEncoder.encode(title, Charsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			return title;
		}
	}

}
