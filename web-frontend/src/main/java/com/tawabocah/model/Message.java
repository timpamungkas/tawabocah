package com.tawabocah.model;

import lombok.Data;

@Data
public class Message {

	private String email;
	private String content;
	private String name;

}
