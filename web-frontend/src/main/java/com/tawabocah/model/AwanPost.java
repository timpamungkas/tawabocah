package com.tawabocah.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Charsets;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class AwanPost extends BasicAuthenticationPost {

	protected String htmlAdditionalContent;
	protected String htmlMainContent;
	protected long id;
	protected String iframeVideoCode;
	protected String metaDescription;
	protected String metaKeywords;
	protected long nextId;
	protected String originalSource;
	protected long prevId;
	protected Date publishedDate;
	protected String thumbnailAlt;
	protected String thumbnailPath;
	protected String title;
	protected String type;

	public boolean getContainsAdditionalContent() {
		return !StringUtils.isEmpty(htmlAdditionalContent);
	}
	
	public boolean getContainsMainContent() {
		return !StringUtils.isEmpty(htmlMainContent);
	}

	public boolean getHasNextArticle() {
		return getNextId() > 0;
	}

	public boolean getHasPrevArticle() {
		return getPrevId() > 0;
	}

	public boolean getHasThumbnail() {
		return !StringUtils.isEmpty(thumbnailPath);
	}

	public boolean getHasVideo() {
		return !StringUtils.isEmpty(getIframeVideoCode());
	}

	public Set<String> getMetaKeywordSet() {
		Set<String> metaKeywordSet = Sets.newLinkedHashSet();
		for (String s : this.metaKeywords.split(",")) {
			metaKeywordSet.add(StringUtils.trim(s));
		}

		return metaKeywordSet;
	}

	public String getThumbnailPathEncoded() {
		try {
			return URLEncoder.encode(thumbnailPath, Charsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			return thumbnailPath;
		}
	}

}
