package com.tawabocah.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tawabocah.util.StringFormatUtil;

import lombok.Data;

@Data
public class MatahariCatalogItem {

	private String assetPath;
	private String htmlAdditionalContent;
	private String htmlMainContent;
	private long id;
	private String imageAlt;
	private List<MatahariCatalogItemImage> images = Lists.newArrayList();
	private String metaDescription;
	private String metaKeywords;
	private int price;
	private Date publishedDate;
	private String thumbnailPath;
	private String title;

	public boolean getContainsAdditionalContent() {
		return !StringUtils.isEmpty(htmlAdditionalContent);
	}

	public Set<String> getMetaKeywordSet() {
		Set<String> metaKeywordSet = Sets.newLinkedHashSet();
		for (String s : this.metaKeywords.split(",")) {
			metaKeywordSet.add(StringUtils.trim(s));
		}

		return metaKeywordSet;
	}

	// Helper for thymeleaf
	public String getPriceFormatted() {
		return StringFormatUtil.formatCurrency(price);
	}

}
