package com.tawabocah.model;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
public class BasicAuthenticationPost {
	// TODO : move token validation to external, better implementation
	private static final List<String> validTokens = Lists.newArrayList();
	static {
		validTokens.add("54fde6cedd14a9be1732194f4386eac5b53a76adc6580793a936d83c869de1ae");
		validTokens.add("c378b7d8b8fbd43384196901bb1535fcbdbe0e3eeae979ae28a21cc6ef0346d9");
		validTokens.add("f32097180669b161131d892a8b1f2a0f126b33539a0d4bf0080c18b5464c6895");
		validTokens.add("5ccd21f555c1a5e6e21b505fbd2834ef34933f464fc790aca9cf20961cfb4ec7");
	}

	/**
	 * Temporary while no user management
	 */
	protected String token;

	public boolean isValidToken() {
		String tokenEncrypted = DigestUtils.sha256Hex(token);

		return validTokens.contains(tokenEncrypted);
	}

}
