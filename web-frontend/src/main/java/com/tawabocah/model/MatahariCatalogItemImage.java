package com.tawabocah.model;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatahariCatalogItemImage {

	@Required
	private String imagePath;

}
