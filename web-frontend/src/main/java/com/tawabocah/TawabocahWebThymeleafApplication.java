package com.tawabocah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class TawabocahWebThymeleafApplication extends SpringBootServletInitializer {

//	@Bean
//	RestTemplate restTemplate() {
//		return new RestTemplate();
//	}

	public static void main(String[] args) {
		// Open only on localhost when using Spring Boot 1.4 embedded tomcat
//		if (AuthConfigFactory.getFactory() == null) {
//			AuthConfigFactory.setFactory(new AuthConfigFactoryImpl());
//		}

		SpringApplication.run(TawabocahWebThymeleafApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TawabocahWebThymeleafApplication.class);
	}
}
