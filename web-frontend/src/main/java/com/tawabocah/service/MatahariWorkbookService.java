package com.tawabocah.service;

import com.tawabocah.model.MatahariWorkbookItem;

public interface MatahariWorkbookService {
	
	MatahariWorkbookItem getById(Long id);

}
