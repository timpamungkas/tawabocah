package com.tawabocah.service;

import org.springframework.data.domain.Page;

import com.tawabocah.model.MatahariCatalogItem;

public interface MatahariCatalogService {
	
	Page<MatahariCatalogItem> findPublishedPageable(int pageNumber, int pageSize, String sortBy, String grade);

	MatahariCatalogItem getById(Long id);
	
	long save(MatahariCatalogItem item);

}
