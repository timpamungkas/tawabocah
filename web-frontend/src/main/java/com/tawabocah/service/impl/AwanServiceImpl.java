package com.tawabocah.service.impl;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tawabocah.constant.RestConstants;
import com.tawabocah.model.AwanPost;
import com.tawabocah.service.AwanService;

@Service
@Scope("prototype")
public class AwanServiceImpl implements AwanService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Page<AwanPost> findPublishedPageable(int pageNumber, int pageSize, String sortBy) {
		Page<AwanPost> page = null;

		try {
			URIBuilder uriBuilder = new URIBuilder(
					RestConstants.URL + "/post-service/api/v1/post/article/published/list");
			uriBuilder.addParameter("p", Integer.toString(pageNumber));
			uriBuilder.addParameter("ps", Integer.toString(pageSize));
			uriBuilder.addParameter("sb", sortBy);

			AwanPost[] posts = this.restTemplate.getForObject(uriBuilder.build().toString(), AwanPost[].class);
			List<AwanPost> list = Arrays.asList(posts);

			uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/article/published/count");
			long totalPublishedItems = this.restTemplate.getForObject(uriBuilder.build().toString(), Long.class);

			Pageable pageable = new PageRequest(pageNumber - 1, pageSize);
			page = new PageImpl<>(list, pageable, totalPublishedItems);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return page;
	}

	@Override
	public AwanPost getById(Long id) {
		AwanPost awanPost = this.restTemplate
				.getForObject(RestConstants.URL + "/post-service/api/v1/post/article/" + id, AwanPost.class);

		return awanPost;
	}

	@Override
	public long save(AwanPost postItem) {
		long savedId = 0;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/article/save");
			savedId = this.restTemplate.postForObject(uriBuilder.build().toString(), postItem, Long.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return savedId;
	}

}
