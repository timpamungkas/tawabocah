package com.tawabocah.service.impl;

import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tawabocah.constant.RestConstants;
import com.tawabocah.model.Message;
import com.tawabocah.service.ContactUsService;

@Service
public class ContactUsServiceImpl implements ContactUsService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public boolean saveAndSendMail(Message message) {
		boolean isSaved = false;
		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/contact/save");

			isSaved = restTemplate.postForObject(uriBuilder.build().toString(), message, Boolean.class);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return isSaved;
	}

}
