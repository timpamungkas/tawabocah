package com.tawabocah.service.impl;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tawabocah.constant.RestConstants;
import com.tawabocah.model.BulanPost;
import com.tawabocah.service.BulanService;

@Service
@Scope("prototype")
public class BulanServiceImpl implements BulanService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Page<BulanPost> findPublishedPageable(int pageNumber, int pageSize, String sortBy) {
		Page<BulanPost> page = null;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/worksheet/published/list");
			uriBuilder.addParameter("p", Integer.toString(pageNumber));
			uriBuilder.addParameter("ps", Integer.toString(pageSize));
			uriBuilder.addParameter("sb", sortBy);

			BulanPost[] posts = this.restTemplate.getForObject(uriBuilder.build().toString(), BulanPost[].class);
			List<BulanPost> list = Arrays.asList(posts);

			uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/worksheet/published/count");
			long totalPublishedItems = this.restTemplate.getForObject(uriBuilder.build().toString(), Long.class);

			Pageable pageable = new PageRequest(pageNumber - 1, pageSize);
			page = new PageImpl<>(list, pageable, totalPublishedItems);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return page;
	}

	@Override
	public BulanPost getById(Long id) {
		BulanPost bulanPost = this.restTemplate.getForObject(RestConstants.URL + "/post-service/api/v1/post/worksheet/" + id,
				BulanPost.class);

		return bulanPost;
	}

	@Override
	public long save(BulanPost postItem) {
		long savedId = 0;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/worksheet/save");
			savedId = this.restTemplate.postForObject(uriBuilder.build().toString(), postItem, Long.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return savedId;
	}

}
