package com.tawabocah.service.impl;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tawabocah.constant.RestConstants;
import com.tawabocah.model.MatahariCatalogItem;
import com.tawabocah.service.MatahariCatalogService;

@Service
@Scope("prototype")
public class MatahariCatalogServiceImpl implements MatahariCatalogService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Page<MatahariCatalogItem> findPublishedPageable(int pageNumber, int pageSize, String sortBy, String grade) {
		Page<MatahariCatalogItem> page = null;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/purchase-service/api/v1/workbook/catalog/published/list");
			uriBuilder.addParameter("p", Integer.toString(pageNumber));
			uriBuilder.addParameter("ps", Integer.toString(pageSize));
			uriBuilder.addParameter("sb", sortBy);
			uriBuilder.addParameter("g", grade);

			MatahariCatalogItem[] posts = this.restTemplate.getForObject(uriBuilder.build().toString(),
					MatahariCatalogItem[].class);
			List<MatahariCatalogItem> list = Arrays.asList(posts);

			uriBuilder = new URIBuilder(RestConstants.URL + "/purchase-service/api/v1/workbook/catalog/published/count");
			long totalPublishedItems = this.restTemplate.getForObject(uriBuilder.build().toString(), Long.class);

			Pageable pageable = new PageRequest(pageNumber - 1, pageSize);
			page = new PageImpl<>(list, pageable, totalPublishedItems);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return page;
	}

	@Override
	public MatahariCatalogItem getById(Long id) {
		MatahariCatalogItem matahariCatalogItem = this.restTemplate
				.getForObject(RestConstants.URL + "/purchase-service/api/v1/workbook/catalog/" + id, MatahariCatalogItem.class);

		return matahariCatalogItem;
	}

	@Override
	public long save(MatahariCatalogItem item) {
		long savedId = 0;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/purchase-service/api/v1/workbook/post/save");
			savedId = this.restTemplate.postForObject(uriBuilder.build().toString(), item, Long.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return savedId;	}
}
