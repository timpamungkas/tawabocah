package com.tawabocah.service.impl;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tawabocah.constant.RestConstants;
import com.tawabocah.model.PohonPost;
import com.tawabocah.service.PohonService;

@Service
@Scope("prototype")
public class PohonServiceImpl implements PohonService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Page<PohonPost> findPublishedPageable(int pageNumber, int pageSize, String sortBy) {
		Page<PohonPost> page = null;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/lesson-plan/published/list");
			uriBuilder.addParameter("p", Integer.toString(pageNumber));
			uriBuilder.addParameter("ps", Integer.toString(pageSize));
			uriBuilder.addParameter("sb", sortBy);

			PohonPost[] posts = this.restTemplate.getForObject(uriBuilder.build().toString(), PohonPost[].class);
			List<PohonPost> list = Arrays.asList(posts);

			uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/lesson-plan/published/count");
			long totalPublishedItems = this.restTemplate.getForObject(uriBuilder.build().toString(), Long.class);

			Pageable pageable = new PageRequest(pageNumber - 1, pageSize);
			page = new PageImpl<>(list, pageable, totalPublishedItems);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return page;
	}

	@Override
	public PohonPost getById(Long id) {
		PohonPost pohonPost = this.restTemplate.getForObject(RestConstants.URL + "/post-service/api/v1/post/lesson-plan/" + id,
				PohonPost.class);

		return pohonPost;
	}

	@Override
	public long save(PohonPost postItem) {
		long savedId = 0;

		try {
			URIBuilder uriBuilder = new URIBuilder(RestConstants.URL + "/post-service/api/v1/post/lesson-plan/save");
			savedId = this.restTemplate.postForObject(uriBuilder.build().toString(), postItem, Long.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return savedId;
	}

}
