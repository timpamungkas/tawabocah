package com.tawabocah.service;

import org.springframework.data.domain.Page;

public interface PostService<T> {

	Page<T> findPublishedPageable(int evalPage, int evalPageSize, String evalSortBy);

	T getById(Long id);

	long save(T postItem);
	
}
