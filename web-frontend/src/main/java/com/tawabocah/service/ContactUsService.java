package com.tawabocah.service;

import com.tawabocah.model.Message;

public interface ContactUsService {

	boolean saveAndSendMail(Message message);

}
