$(document).ready(function() {
	changeParameters();
});

// change request param
function changeParameters() {
	$('#pageSizeSelect, #sortBySelect').change(
			function(evt) {
				var currentURL = location.protocol + '//' + location.host
						+ location.pathname;
				var sortBy = $('#sortBySelect :selected').val();
				var pageSize = $('#pageSizeSelect :selected').val();
				var grade = $("#lstGrade li.active").data("grade");

				if (grade == null || grade == undefined || grade.length == 0) {
					window.location.replace(currentURL + "?ps=" + pageSize
							+ "&p=1" + "&sb=" + sortBy);
				} else {
					window.location.replace(currentURL + "?ps=" + pageSize
							+ "&p=1" + "&sb=" + sortBy + "&g=" + grade);
				}
			});
}
// End change request param

function toCurrencyString(amountNum) {
	var rev = parseInt(amountNum, 10).toString().split('').reverse().join('');
	var rev2 = '';
	for (var i = 0; i < rev.length; i++) {
		rev2 += rev[i];
		if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
			rev2 += '.';
		}
	}
	return 'Rp ' + rev2.split('').reverse().join('');
}

function toCurrencyNum(currencyString) {
	return parseInt(currencyString.replace(/,.*|\D/g, ''), 10)
}
