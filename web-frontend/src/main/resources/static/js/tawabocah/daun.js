jQuery(document).ready(function($) {
	$("#frmNew").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		submitNewPost();
	});
});

function submitNewPost() {
	var data = {};
	data["token"] = $("#token").val();
	data["htmlActivityContent"] = $("#htmlActivityContent").summernote(
			"code");
	data["htmlAdditionalContent"] = $("#htmlAdditionalContent").summernote("code");
	data["htmlOverviewContent"] = $("#htmlOverviewContent").summernote("code");
	data["htmlRequirementContent"] = $("#htmlRequirementContent").summernote("code");
	data["assetPath"] = $("#assetPath").val();
	data["grade"] = $("#grade").val();
	data["imageAlt"] = $("#imageAlt").val();
	data["imagePath"] = $("#imagePath").val();
	data["metaDescription"] = $("#metaDescription").val();
	data["metaKeywords"] = $("#metaKeywords").val();
	data["originalSource"] = $("#originalSource").val();
	data["thumbnailPath"] = $("#thumbnailPath").val();
	data["title"] = $("#title").val();

//	$("#btnSubmit").prop("disabled", true);

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/aktivitas-anak/daun/baru",
		data : JSON.stringify(data),
		timeout : 100000,
		success : function(data) {
			document.open();
			document.write(data);
			document.close();

			$("#btnSubmit").prop("disabled", false);
			
			window.location.replace("/aktivitas-anak/daun");
		},
		fail : function(e) {
			console.log("FAIL: ", e);
			alert("Fail");
		},
		error : function(e) {
			console.log("ERROR: ", e);
			alert("Error");
		}
	});
}