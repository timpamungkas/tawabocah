jQuery(document).ready(function($) {
	$("#frmNew").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		submitNewPost();
	});
});

function submitNewPost() {
	var data = {};
	data["token"] = $("#token").val();
	data["htmlAdditionalContent"] = $("#htmlAdditionalContent").summernote(
			"code");
	data["htmlMainContent"] = $("#htmlMainContent").summernote("code");
	data["iframeVideoCode"] = $("#iframeVideoCode").val();
	data["metaDescription"] = $("#metaDescription").val();
	data["metaKeywords"] = $("#metaKeywords").val();
	data["originalSource"] = $("#originalSource").val();
	data["thumbnailAlt"] = $("#thumbnailAlt").val();
	data["thumbnailPath"] = $("#thumbnailPath").val();
	data["type"] = $("#type").val();
	data["title"] = $("#title").val();

	$("#btnSubmit").prop("disabled", true);

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/aktivitas-anak/awan/baru",
		data : JSON.stringify(data),
		timeout : 100000,
		success : function(data) {
			document.open();
			document.write(data);
			document.close();

			$("#btnSubmit").prop("disabled", false);
			
			window.location.replace("/aktivitas-anak/awan");
		},
		fail : function(e) {
			console.log("FAIL: ", e);
			alert("Fail");
		},
		error : function(e) {
			console.log("ERROR: ", e);
			alert("Error");
		}
	});
}