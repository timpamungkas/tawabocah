package com.tawabocah.domain;

import org.springframework.core.io.InputStreamSource;

import lombok.Data;

/**
 * Mail attachment for sending mail
 * 
 * @author Timotius Pamungkas
 *
 */
@Data
public class MailAttachment implements Comparable<MailAttachment> {

	private String attachmentFilename;
	private InputStreamSource inputStreamSource;
	private String contentType;

	public MailAttachment(String attachmentFilename, InputStreamSource inputStreamSource, String contentType) {
		super();
		this.attachmentFilename = attachmentFilename;
		this.inputStreamSource = inputStreamSource;
		this.contentType = contentType;
	}

	@Override
	public int compareTo(MailAttachment o) {
		return this.attachmentFilename.compareTo(o.attachmentFilename);
	}

}
