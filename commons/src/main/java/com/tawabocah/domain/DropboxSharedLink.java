package com.tawabocah.domain;

import java.util.Date;

import lombok.Data;

@Data
public class DropboxSharedLink {

	private Date expires;
	private String url;
	private String visibility;
	
}
