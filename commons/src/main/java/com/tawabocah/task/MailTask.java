package com.tawabocah.task;

import java.util.List;
import java.util.concurrent.Callable;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;

import com.tawabocah.constants.TaskStatus;
import com.tawabocah.domain.MailAttachment;
import com.tawabocah.util.MailUtil;

import lombok.Data;

@Data
public class MailTask implements Callable<TaskStatus> {

	private List<MailAttachment> attachments;

	private String body;
	private boolean html;
	@Autowired
	private MailUtil mailUtil;
	private String subject;
	private String to;
	private String cc;
	private String bcc;

	public MailTask(String to, String cc, String bcc, String subject, String body, boolean html) {
		super();
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject;
		this.body = body;
		this.html = html;
	}

	@Override
	public TaskStatus call() throws Exception {
		try {
			mailUtil.send(to, cc, bcc, subject, body, html,
					attachments.toArray(new MailAttachment[attachments.size()]));
		} catch (MessagingException e) {
			return TaskStatus.FAILED;
		}

		return TaskStatus.SUCCESS;
	}

}
