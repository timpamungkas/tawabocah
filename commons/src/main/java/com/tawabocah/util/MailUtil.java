package com.tawabocah.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.tawabocah.domain.MailAttachment;

@Component
public class MailUtil {

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${spring.mail.username}")
	public String me;

	/**
	 * Send plain (text email)
	 * 
	 * @param to
	 *            recipients
	 * @param subject
	 *            email subject
	 * @param body
	 *            plain text body
	 * @throws MessagingException
	 */
	public void send(String to, String cc, String bcc, String subject, String body) throws MessagingException {
		send(to, cc, bcc, subject, body, false);
	}

	public void send(String to, String cc, String bcc, String subject, String body, boolean isHtmlContent, MailAttachment... attachments)
			throws MessagingException {
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);

		messageHelper.setTo(to);
		messageHelper.setSubject(subject);
		messageHelper.setText(body, isHtmlContent);
		
		if (!StringUtils.isEmpty(cc)) {
			messageHelper.setCc(cc);
		}

		if (!StringUtils.isEmpty(bcc)) {
			messageHelper.setBcc(bcc);
		}

		for (MailAttachment attachment : attachments) {
			messageHelper.addAttachment(attachment.getAttachmentFilename(), attachment.getInputStreamSource(),
					attachment.getContentType());
		}

		javaMailSender.send(message);
	}

}
