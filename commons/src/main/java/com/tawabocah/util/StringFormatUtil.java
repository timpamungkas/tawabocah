package com.tawabocah.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

public class StringFormatUtil {

	private static final Locale LOCALE_INDONESIA = new Locale("in", "ID");
	private static final String CURRENCY_CODE_IDR = Currency.getInstance(LOCALE_INDONESIA).getSymbol(LOCALE_INDONESIA)
			+ StringUtils.SPACE;
	private static final DateFormat dateFormatLong = new SimpleDateFormat("EEEEE, dd-MMMMM-yyyy", LOCALE_INDONESIA);
	private static final DateFormat dateFormatShort = new SimpleDateFormat("dd-MMM-yyyy", LOCALE_INDONESIA);
	private static final DateFormat datetimeFormatLong = new SimpleDateFormat("EEEEE, dd-MMMMM-yyyy HH:mm",
			LOCALE_INDONESIA);
	private static final DateFormat datetimeFormatShort = new SimpleDateFormat("dd-MMM-yyyy HH:mm", LOCALE_INDONESIA);
	private static final NumberFormat decimalFormat = DecimalFormat.getInstance(LOCALE_INDONESIA);
	private static final NumberFormat integerFormat = DecimalFormat.getIntegerInstance(LOCALE_INDONESIA);

	public static int currencyToIntValue(String currencyStr) throws ParseException {
		int result = 0;
		try {
			String numericPartString = currencyStr.substring(CURRENCY_CODE_IDR.length());
			result = integerFormat.parse(numericPartString).intValue();
		} catch (StringIndexOutOfBoundsException ex) {
			result = 0;
		}

		return result;
	}

	public static String formatCurrency(Integer d) {
		// default java format : Rp xxxxxx,00 (ada decimal nya), override
		return new StringBuilder().append(CURRENCY_CODE_IDR).append(formatInteger(d)).toString();
	}

	public static String formatDateLong(Calendar cal) {
		return dateFormatLong.format(cal.getTime());
	}

	public static String formatDateLong(Date d) {
		return dateFormatLong.format(d);
	}

	public static String formatDateShort(Calendar cal) {
		return dateFormatShort.format(cal.getTime());
	}

	public static String formatDateShort(Date d) {
		return dateFormatShort.format(d);
	}

	public static String formatDateTimeLong(Calendar cal) {
		return datetimeFormatLong.format(cal.getTime());
	}

	public static String formatDateTimeLong(Date d) {
		return datetimeFormatLong.format(d);
	}

	public static String formatDateTimeShort(Calendar cal) {
		return datetimeFormatShort.format(cal.getTime());
	}

	public static String formatDateTimeShort(Date d) {
		return datetimeFormatShort.format(d);
	}

	public static String formatDecimal(Number d) {
		return decimalFormat.format(d);
	}

	public static String formatInteger(Number d) {
		return DecimalFormat.getIntegerInstance(LOCALE_INDONESIA).format(d);
	}

	/**
	 * 1 disini tetap akan jadi 1 (100%), bukan decimal format biasa, jadi ambil
	 * int nya (kalo default java, ambil float nya)
	 * 
	 * @param d
	 * @return
	 */
	public static String formatPercent(Number d) {
		return new StringBuilder(integerFormat.format(d)).append('%').toString();
	}

}
