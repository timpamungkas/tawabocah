package com.tawabocah.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.StringRenderer;

/**
 * Helper for StringTemplate
 * 
 * @author Timotius Pamungkas
 *
 */
public class StringTemplateUtil {

	public static final char DEFAULT_DELIMITER = '$';

	public String createFromST(String templatePath, char delimiter, Map<String, Object> values) throws IOException {
		InputStream templateStream;

		templateStream = StringTemplateUtil.class.getClassLoader().getResourceAsStream(templatePath);
		if (templateStream == null) {
			templateStream = new URL(templatePath).openStream();
		}

		final String myString = IOUtils.toString(templateStream, StandardCharsets.UTF_8.name());
		final ST st = new ST(myString, delimiter, delimiter);

		if (values != null && !values.isEmpty()) {
			for (String key : values.keySet()) {
				Object value = values.get(key);
				st.add(key, value);
			}
		}

		return st.render();
	}

	public String createFromSTGroup(URL url, String templateName, String encoding, char delimiter,
			Map<String, Object> values) {
		final STGroup stGroup = new STGroupFile(url, encoding, delimiter, delimiter);
		stGroup.registerRenderer(String.class, new StringRenderer());
		final ST st = stGroup.getInstanceOf(templateName);

		if (values != null && !values.isEmpty()) {
			for (String key : values.keySet()) {
				Object value = values.get(key);
				st.add(key, value);
			}
		}

		return st.render();
	}

}
