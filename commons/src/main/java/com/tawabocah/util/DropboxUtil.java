package com.tawabocah.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxClientV1;
import com.dropbox.core.v1.DbxDelta;
import com.dropbox.core.v1.DbxEntry;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.dropbox.core.v2.sharing.CreateSharedLinkWithSettingsErrorException;
import com.dropbox.core.v2.sharing.DbxUserSharingRequests;
import com.dropbox.core.v2.sharing.RevokeSharedLinkErrorException;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.tawabocah.constants.GlobalConstants;
import com.tawabocah.domain.DropboxSharedLink;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Getter
public class DropboxUtil {

	/**
	 * Parse dropbox into direct URL that can be referenced from HTML as
	 * image/file
	 * 
	 * @param original
	 *            original dropbox address
	 * @return parsed URL
	 */
	public static String parseDropboxUserContent(String original) {
		String dbuserContent = StringUtils.replace(original, "www.dropbox.com", "dl.dropboxusercontent.com");
		String noRequestParam = StringUtils.substring(dbuserContent, 0, dbuserContent.lastIndexOf('?')).trim();

		return noRequestParam;
	}

	private DbxClientV1 dbxClientV1;
	private DbxClientV2 dbxClientV2;
	private DbxUserSharingRequests dbxSharing;
	private String dropboxOauthToken;

	public DropboxUtil() {
		URL dropboxSecret = DropboxUtil.class.getClassLoader().getResource("dropbox.secret");
		URL dropboxOauthSecret = DropboxUtil.class.getClassLoader().getResource("dropbox-oauth.secret");

		String dropboxToken = null;
		try {
			dropboxToken = IOUtils.toString(dropboxSecret.openStream(), StandardCharsets.UTF_8.name());
			dropboxOauthToken = IOUtils.toString(dropboxOauthSecret.openStream(), StandardCharsets.UTF_8.name());

			// Create Dropbox client
			DbxRequestConfig config = DbxRequestConfig.newBuilder("").build();
			this.dbxClientV1 = new DbxClientV1(config, dropboxToken);
			this.dbxClientV2 = new DbxClientV2(config, dropboxToken);
			this.dbxSharing = this.dbxClientV2.sharing();
		} catch (IOException e) {
			log.error("Can't create dropbox client: " + e.getMessage());
		}
	}

	public SharedLinkMetadata createSharedLink(String dbxPath)
			throws CreateSharedLinkWithSettingsErrorException, DbxException {
		return dbxSharing.createSharedLinkWithSettings(dbxPath);
	}

	/**
	 * Download file from dropbox
	 * 
	 * @param dbxPath
	 *            qualified dropbox path
	 * @param toTargetFile
	 *            File to save
	 * @return
	 * @throws IOException
	 * @throws DbxException
	 */
	public boolean downloadFile(String dbxPath, File toTargetFile) throws IOException, DbxException {
		// check if this is a file
		DbxEntry dbxEntry = getDbxClientV1().getMetadata(dbxPath);

		// checked exception
		if (dbxEntry == null) {
			throw new DbxException("Path does not exists : " + dbxPath);
		}

		// checked exception
		if (!dbxEntry.isFile()) {
			throw new DbxException("Can't download dropbox Folder : " + dbxPath);
		}

		DbxEntry.File dbxEntryFile;
		OutputStream out = new FileOutputStream(toTargetFile);

		try {
			dbxEntryFile = getDbxClientV1().getFile(dbxPath, null, out);
		} finally {
			out.close();
		}

		return dbxEntryFile == null ? false : true;
	}

	public DbxClientV1 getDbxClientV1() {
		return dbxClientV1;
	}

	public InputStream getDbxInputStream(DbxEntry f) throws DbxException {
		return getDbxClientV1().startGetFile(f.path, null).body;
	}

	public List<DbxEntry> list(String dbxPath) throws DbxException {
		List<DbxEntry> result = new ArrayList<>();
		String cursor = null;

		DbxDelta<DbxEntry> delta = getDbxClientV1().getDeltaWithPathPrefix(cursor, dbxPath);
		for (DbxDelta.Entry<DbxEntry> e : delta.entries) {
			result.add(e.metadata);
		}

		return result;
	}

	public void revokeSharedLink(String dbxPath) throws RevokeSharedLinkErrorException, DbxException {
		dbxSharing.revokeSharedLink(dbxPath);
	}

	/**
	 * 
	 * @param fileToUpload
	 * @param dbxPathToUpload
	 *            dropbox path (full, include file extension)
	 * @return
	 * @throws UploadErrorException
	 * @throws FileNotFoundException
	 * @throws DbxException
	 * @throws IOException
	 */
	public FileMetadata upload(File fileToUpload, String dbxPathToUpload)
			throws UploadErrorException, FileNotFoundException, DbxException, IOException {
		if (!StringUtils.startsWith(dbxPathToUpload, Character.toString(GlobalConstants.URL_PATH_SEPARATOR))) {
			dbxPathToUpload = new StringBuilder().append(GlobalConstants.URL_PATH_SEPARATOR).append(dbxPathToUpload)
					.toString();
		}

		return getDbxClientV2().files().uploadBuilder(dbxPathToUpload)
				.uploadAndFinish(new FileInputStream(fileToUpload));
	}

	public FileMetadata upload(String fileToUpload, String dbxPathToUpload)
			throws UploadErrorException, FileNotFoundException, DbxException, IOException {
		return this.upload(new File(fileToUpload), dbxPathToUpload);
	}

	/**
	 * Return dropbox shared link metadata from drobox end point REST. Will
	 * returns URL in shortened path.
	 * 
	 * @param dbxPath
	 * @return
	 */
	public DropboxSharedLink getDropboxSharedLink(String dbxPath) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Bearer " + getDropboxOauthToken());
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<DropboxSharedLink> response = restTemplate.exchange(
				"https://api.dropboxapi.com/1/shares/auto/" + dbxPath, HttpMethod.GET, httpEntity,
				DropboxSharedLink.class);

		return response.getBody();
	}

}
