package com.tawabocah.constants;

public enum FileExtension {
	JPG_WITH_DOT(".jpg"), PDF_WITH_DOT(".pdf");

	private String extension;

	private FileExtension(String extension) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

}