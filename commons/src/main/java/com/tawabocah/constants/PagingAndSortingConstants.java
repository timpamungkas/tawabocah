package com.tawabocah.constants;

import java.util.LinkedHashMap;
import java.util.Map;

public final class PagingAndSortingConstants {

	// paging
	public static final int BUTTONS_TO_SHOW = 5;
	public static final int INITIAL_PAGE = 1;
	public static final int INITIAL_PAGE_SIZE = 10;
	public static final int[] PAGE_SIZES = { 10, 20, 40 };

	// sorting
	public static final Map<String, String> SORT_BY_OPTIONS = new LinkedHashMap<>();
	public static final String INITIAL_SORT_BY = "dateNewest";

	static {
		SORT_BY_OPTIONS.put("dateNewest", "Tanggal (baru ke lama)");
		SORT_BY_OPTIONS.put("dateOldest", "Tanggal (lama ke baru)");
		SORT_BY_OPTIONS.put("titleAZ", "Judul (A ke Z)");
		SORT_BY_OPTIONS.put("titleZA", "Judul (Z ke A)");
		SORT_BY_OPTIONS.put("random", "Acak");
	}

}
