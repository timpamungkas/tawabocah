package com.tawabocah.constants;

/**
 * Process status for matahari
 * @author Timotius Pamungkas
 *
 */
public enum ProcessStatus {

	ACCEPTED("PO diterima"), CANCEL(
			"Transaksi batal"), COMPLETE("Sudah dikirim"), GENERATED("Siap dikirim"), NEW("Belum diproses");

	private final String description;

	ProcessStatus(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
