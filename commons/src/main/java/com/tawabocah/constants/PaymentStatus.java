package com.tawabocah.constants;

/**
 * Payment status for matahari
 * @author Timotius Pamungkas
 *
 */
public enum PaymentStatus {

	CANCEL("Transaksi batal"), NEW("Belum dibayar"), PAID("Sudah dibayar");

	private final String description;

	PaymentStatus(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
