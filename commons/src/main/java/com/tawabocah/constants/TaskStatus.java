package com.tawabocah.constants;

/**
 * General task status for callable
 * @author Timotius Pamungkas
 *
 */
public enum TaskStatus {

	FAILED((byte) 0), SUCCESS((byte) 1);

	private final byte value;

	TaskStatus(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}
}
