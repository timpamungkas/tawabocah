package com.tawabocah.constants;

public interface GlobalConstants {

	String CLASS_PATH_XML_APPLICATION_CONTEXT = "application-context.xml";

	int FIXED_THREAD_POOL_SIZE = Runtime.getRuntime().availableProcessors() > 2
			? Runtime.getRuntime().availableProcessors() : 2;
	String REGEX_BLANK = "\\p{Blank}";
	String REGEX_BLANK_STRING_REPLACEMENT = "-";

	String ZK_SORT_BY_DATE_NEWEST = "dateNewest";
	String ZK_SORT_BY_DATE_OLDEST = "dateOldest";
	String ZK_SORT_BY_TITLE_AZ = "titleAZ";
	String ZK_SORT_BY_TITLE_ZA = "titleZA";
	String ZK_SORT_BY_RANDOM = "random";

	char URL_PATH_SEPARATOR = '/';
}
