package com.tawabocah.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.WorkbookCatalogItem;

@Repository
public interface WorkbookCatalogItemRepository extends PagingAndSortingRepository<WorkbookCatalogItem, Long> {

}
