package com.tawabocah.spring.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PurchaseOrderHdr;

@Repository
@Scope("prototype")
public interface PurchaseOrderRepository extends PagingAndSortingRepository<PurchaseOrderHdr, Long> {

}
