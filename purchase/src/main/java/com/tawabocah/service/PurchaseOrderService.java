package com.tawabocah.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tawabocah.hibernate.domain.PurchaseOrderHdr;

public interface PurchaseOrderService {

	public long count();

	public void delete(Iterable<PurchaseOrderHdr> purchaseOrders);

	public void delete(Long id);

	public void delete(PurchaseOrderHdr purchaseOrder);

	public boolean exists(Long id);

	public Iterable<PurchaseOrderHdr> findAll();

	public Iterable<PurchaseOrderHdr> findAll(Iterable<Long> ids);

	public Page<PurchaseOrderHdr> findAll(Pageable pageable);

	public PurchaseOrderHdr findOne(Long id);

	public Iterable<PurchaseOrderHdr> save(Iterable<PurchaseOrderHdr> purchaseOrders);

	public PurchaseOrderHdr save(PurchaseOrderHdr purchaseOrder);

}
