package com.tawabocah.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tawabocah.hibernate.domain.WorkbookCatalogItem;

public interface WorkbookCatalogItemService {

	long count();

	long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags);

	void delete(Iterable<WorkbookCatalogItem> workbookCatalogItems);

	void delete(Long id);

	void delete(WorkbookCatalogItem workbookCatalogItem);

	boolean exists(Long id);

	Iterable<WorkbookCatalogItem> findAll();

	Iterable<WorkbookCatalogItem> findAll(Iterable<Long> ids);

	Page<WorkbookCatalogItem> findAll(Pageable pageable);

	WorkbookCatalogItem findOne(Long id);

	List<WorkbookCatalogItem> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders);

	Iterable<WorkbookCatalogItem> save(Iterable<WorkbookCatalogItem> WorkbookCatalogItems);

	WorkbookCatalogItem save(WorkbookCatalogItem workbookCatalogItem);

	void saveOrUpdate(WorkbookCatalogItem workbookCatalogItem);

	void update(WorkbookCatalogItem workbookCatalogItem);

	List<WorkbookCatalogItem> findPublishedPageable(int evalPage, int evalPageSize, String evalSortBy, String grade);

}
