package com.tawabocah.service.impl;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.dao.WorkbookCatalogItemDAO;
import com.tawabocah.hibernate.domain.WorkbookCatalogItem;
import com.tawabocah.hibernate.domain.WorkbookCatalogItemImage;
import com.tawabocah.service.WorkbookCatalogItemService;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class WorkbookCatalogItemServiceImpl implements WorkbookCatalogItemService {

	@Autowired
	private WorkbookCatalogItemDAO workbookCatalogItemDAO;

	@Override
	public long count() {
		return workbookCatalogItemDAO.count();
	}

	@Override
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) {
		long result = 0;

		try {
			result = workbookCatalogItemDAO.count(publishedDateFrom, publishedDateTo, tags);
		} catch (SQLException e) {
			log.error("Error count catalog item : " + e.getMessage());
		}

		return result;
	}

	@Override
	public void delete(Iterable<WorkbookCatalogItem> workbookCatalogItems) {
		workbookCatalogItemDAO.delete(workbookCatalogItems);
	}

	@Override
	public void delete(Long id) {
		workbookCatalogItemDAO.delete(id);
	}

	@Override
	public void delete(WorkbookCatalogItem workbookCatalogItem) {
		workbookCatalogItemDAO.delete(workbookCatalogItem);
	}

	@Override
	public boolean exists(Long id) {
		return workbookCatalogItemDAO.exists(id);
	}

	@Override
	public Iterable<WorkbookCatalogItem> findAll() {
		return workbookCatalogItemDAO.findAll();
	}

	@Override
	public Iterable<WorkbookCatalogItem> findAll(Iterable<Long> ids) {
		return workbookCatalogItemDAO.findAll(ids);
	}

	@Override
	public Page<WorkbookCatalogItem> findAll(Pageable pageable) {
		return workbookCatalogItemDAO.findAll(pageable);
	}

	@Override
	public WorkbookCatalogItem findOne(Long id) {
		return workbookCatalogItemDAO.findOne(id);
	}

	@Override
	@Transactional
	public List<WorkbookCatalogItem> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) {
		try {
			return workbookCatalogItemDAO.findAll(fetchHtmlContent, publishedDateFrom, publishedDateTo, tags,
					startIndex, limit, random, orders);
		} catch (SQLException e) {
			log.error("Can't findAll: " + e.getMessage());
		}

		return Lists.newArrayList();
	}

	@Override
	public Iterable<WorkbookCatalogItem> save(Iterable<WorkbookCatalogItem> workbookCatalogItems) {
		for (WorkbookCatalogItem workbookCatalogItem : workbookCatalogItems) {
			for (WorkbookCatalogItemImage image : workbookCatalogItem.getImages()) {
				image.setWorkbookCatalogItem(workbookCatalogItem);
			}
		}

		return workbookCatalogItemDAO.save(workbookCatalogItems);
	}

	@Override
	public WorkbookCatalogItem save(WorkbookCatalogItem workbookCatalogItem) {
		for (WorkbookCatalogItemImage image : workbookCatalogItem.getImages()) {
			image.setWorkbookCatalogItem(workbookCatalogItem);
		}

		return workbookCatalogItemDAO.save(workbookCatalogItem);
	}

	@Override
	public void saveOrUpdate(WorkbookCatalogItem workbookCatalogItem) {
		try {
			workbookCatalogItemDAO.saveOrUpdate(workbookCatalogItem);
		} catch (SQLException e) {
			log.error("Can't saveOrUpdate workbook catalog item : " + e.getMessage());
		}
	}

	@Override
	public void update(WorkbookCatalogItem workbookCatalogItem) {
		try {
			workbookCatalogItemDAO.update(workbookCatalogItem);
		} catch (SQLException e) {
			log.error("Can't update workbook catalog item : " + e.getMessage());
		}
	}

	@Override
	public List<WorkbookCatalogItem> findPublishedPageable(int pageNumber, int pageSize, String sortBy, String grade) {
		Order sortByParam = Order.desc("publishedDate");
		Order sortbyIdDesc = Order.desc("id");
		boolean random = false;

		switch (sortBy) {
		case "dateNewest":
			sortByParam = Order.desc("publishedDate");
			break;
		case "dateOldest":
			sortByParam = Order.asc("publishedDate");
			break;
		case "titleAZ":
			sortByParam = Order.asc("title");
			break;
		case "titleZA":
			sortByParam = Order.desc("title");
			break;
		case "random":
			random = true;
			break;
		}

		Date publishedDateTo = Calendar.getInstance().getTime();
		List<String> grades = null;

		if (!StringUtils.isEmpty(grade)) {
			grades = Lists.newArrayListWithCapacity(1);
			grades.add(grade);
		}

		List<WorkbookCatalogItem> list = findAll(true, null, publishedDateTo, grades, (pageNumber - 1) * pageSize,
				pageSize, random, sortByParam, sortbyIdDesc);

		return list;
	}

}
