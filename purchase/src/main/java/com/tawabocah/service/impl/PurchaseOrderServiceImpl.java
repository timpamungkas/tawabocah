package com.tawabocah.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tawabocah.hibernate.dao.PurchaseOrderDAO;
import com.tawabocah.hibernate.domain.PurchaseOrderHdr;
import com.tawabocah.service.PurchaseOrderService;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

	@Autowired
	PurchaseOrderDAO purchaseOrderDAO;

	@Override
	public long count() {
		return purchaseOrderDAO.count();
	}

	@Override
	public void delete(Iterable<PurchaseOrderHdr> purchaseOrders) {
		purchaseOrderDAO.delete(purchaseOrders);
	}

	@Override
	public void delete(Long id) {
		purchaseOrderDAO.delete(id);
	}

	@Override
	public void delete(PurchaseOrderHdr purchaseOrder) {
		purchaseOrderDAO.delete(purchaseOrder);
	}

	@Override
	public boolean exists(Long id) {
		return purchaseOrderDAO.exists(id);
	}

	@Override
	public Iterable<PurchaseOrderHdr> findAll() {
		return purchaseOrderDAO.findAll();
	}

	@Override
	public Iterable<PurchaseOrderHdr> findAll(Iterable<Long> ids) {
		return purchaseOrderDAO.findAll(ids);
	}

	@Override
	public Page<PurchaseOrderHdr> findAll(Pageable pageable) {
		return purchaseOrderDAO.findAll(pageable);
	}

	@Override
	public PurchaseOrderHdr findOne(Long id) {
		return purchaseOrderDAO.findOne(id);
	}

	@Override
	public Iterable<PurchaseOrderHdr> save(Iterable<PurchaseOrderHdr> purchaseOrders) {
		return purchaseOrderDAO.save(purchaseOrders);
	}

	@Override
	public PurchaseOrderHdr save(PurchaseOrderHdr purchaseOrder) {
		return purchaseOrderDAO.save(purchaseOrder);
	}

}
