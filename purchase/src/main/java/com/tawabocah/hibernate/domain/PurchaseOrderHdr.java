package com.tawabocah.hibernate.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
@Entity(name = "po_hdr")
public class PurchaseOrderHdr implements Serializable {

	private static final long serialVersionUID = -6441490613909408399L;
	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "creation_date", nullable = false)
	private Date creationDate;

	/**
	 * 0 to 100
	 */
	@Column(name = "discount", length = 3)
	private byte discount;

	@Column(length = 60, nullable = false)
	private String email;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "po_hdr_id", nullable = false, unique = true)
	private long id;

	@Column(name = "last_update_date", nullable = false)
	private Date lastUpdateDate;

	@Column(name = "last_updated_by")
	private int lastUpdatedBy;

	@Column(name = "payment_due_date")
	private Date paymentDueDate;

	/**
	 * Payment status, apakah sudah diterima atau belum. Urutannya : NEW > PAID,
	 * atau NEW > CANCEL
	 */
	@Column(name = "payment_status", length = 15)
	private String paymentStatus;

	/**
	 * Must be list, kalo set ga bisa di parse dari excelnya karena surrogate id
	 * nya sama semua saat initialize (=0)
	 */
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "poHeader")
	private List<WorkbookGeneratorOrderDtl> poGeneratorDetails = Lists.newArrayListWithCapacity(5);

	@Column(name = "po_number", length = 20, nullable = false, unique = true)
	private String poNumber;

	@Column(name = "price_admin_fee", length = 4)
	private int priceAdminFee;

	@Column(name = "price_after_disc", length = 8)
	private int priceAfterDisc;

	@Column(name = "price_base", length = 8)
	private int priceBase;

	@Column(name = "price_invoice", length = 8)
	private int priceInvoice;

	/**
	 * Process status, apakah sudah digenerate / sent. Urutannya : NEW >
	 * GENERATED > COMPLETE atau NEW > CANCEL
	 */
	@Column(name = "process_status", length = 15)
	private String processStatus;

	@Column(name = "promo_code", length = 25)
	private String promoCode;

	public void addOrderDetail(WorkbookGeneratorOrderDtl orderDtl) {
		getPoGeneratorDetails().add(orderDtl);

		setPriceBase(getPriceBase() + orderDtl.getPriceBase());
	}

	private void calculatePrices() {
		this.priceAfterDisc = priceBase - getPriceDiscount();
		if (this.priceBase > 0) {
			this.priceInvoice = this.priceAfterDisc + this.priceAdminFee;
		} else {
			// ignore admin fee
			this.priceInvoice = 0;
		}
	}

	public int getPriceDiscount() {
		Double d = Math.floor(getDiscount() * priceBase);
		return d.intValue();
	}

	public boolean isHasDiscount() {
		return discount > 0;
	}

	public void setDiscount(byte discount) {
		this.discount = discount;
		calculatePrices();
	}

	public void setPoGeneratorDetails(List<WorkbookGeneratorOrderDtl> poGeneratorDetails) {
		int totalBasePrice = 0;
		this.poGeneratorDetails = poGeneratorDetails;

		for (WorkbookGeneratorOrderDtl dtl : poGeneratorDetails) {
			totalBasePrice += dtl.getPriceBase();
		}

		setPriceBase(totalBasePrice);
	}

	public void setPriceAdminFee(int priceAdminFee) {
		this.priceAdminFee = priceAdminFee;
		calculatePrices();
	}

	public void setPriceBase(int priceBase) {
		this.priceBase = priceBase;
		calculatePrices();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrderHdr other = (PurchaseOrderHdr) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 59;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
}
