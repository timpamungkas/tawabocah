package com.tawabocah.hibernate.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "po_dtl")
@Data
public class PurchaseOrderDtl implements Serializable {

	private static final long serialVersionUID = 5863224042004185484L;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "creation_date", nullable = false)
	private Date creationDate;

	@Id
	@Column(name = "po_dtl_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "last_update_date", nullable = false)
	private Date lastUpdateDate;

	@Column(name = "last_updated_by")
	private int lastUpdatedBy;

	@Column(name = "pdf_filename", length = 25)
	private String pdfFilename;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_hdr_id", nullable = false)
	private PurchaseOrderHdr poHeader;

	/**
	 * Basic price before discount (if any)
	 */
	@Column(name = "price_base", length = 7)
	private int priceBase;

	/**
	 * Code to workbook
	 */
	@Column(name = "workbook_code", length = 20, nullable = false)
	private String workbookCode;

	/**
	 * Page size (problem only)
	 */
	@Column(name = "workbook_size", length = 4)
	private int workbookSize = 0;

	/**
	 * "GENERATOR" or "CATALOG", refer to matahari
	 */
	@Column(name = "workbook_type", length = 20, nullable = false)
	private String workbookType;

	public PurchaseOrderDtl() {
		/**
		 * Required for ZK datalist
		 */
		this.id = ThreadLocalRandom.current().nextLong(Long.MAX_VALUE / 2, Long.MAX_VALUE);
	}

}
