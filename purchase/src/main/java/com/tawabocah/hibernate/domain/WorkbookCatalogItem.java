package com.tawabocah.hibernate.domain;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Data;

@Data
@Entity
@Table(name = "po_wb_catalog_items")
public class WorkbookCatalogItem {

	@Column(name = "asset_path", length = 255, nullable = false, unique = true)
	private String assetPath;

	@Column(name = "created_by")
	@JsonIgnore
	private int createdBy;

	@Column(name = "creation_date", nullable = false)
	@JsonIgnore
	private Date creationDate;

	@Column
	@JsonIgnore
	private boolean enabled;

	/**
	 * Additional HTML Content
	 */
	@Lob
	@Column(name = "html_additional_content")
	private String htmlAdditionalContent;
	
	/**
	 * Main HTML Content
	 */
	@Lob
	@Column(name = "html_main_content")
	private String htmlMainContent;

	@Id
	@Column(name = "item_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "image_alt", length = 150)
	private String imageAlt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "workbookCatalogItem", cascade = CascadeType.ALL)
	private List<WorkbookCatalogItemImage> images = Lists.newArrayList();

	@Column(name = "last_update_date", nullable = false)
	@JsonIgnore
	private Date lastUpdateDate;

	@Column(name = "last_updated_by")
	@JsonIgnore
	private int lastUpdatedBy;

	@Column(name = "meta_description", length = 255)
	private String metaDescription;

	@Column(name = "meta_keywords", length = 255)
	private String metaKeywords;

	@Transient
	@JsonIgnore
	private final Set<String> metaKeywordSet = Sets.newLinkedHashSet();

	@Column(name = "price", nullable = false)
	private int price;

	@Column(name = "published_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, dd MMM yyyy HH:mm:ss zzz")
	private Date publishedDate;

	@Column(name = "thumbnail_path", length = 255)
	private String thumbnailPath;

	@Column(name = "title", length = 50, nullable = false)
	private String title;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkbookCatalogItem other = (WorkbookCatalogItem) obj;
		if (assetPath == null) {
			if (other.assetPath != null)
				return false;
		} else if (!assetPath.equals(other.assetPath))
			return false;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetPath == null) ? 0 : assetPath.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WorkbookCatalogItem [assetPath=").append(assetPath).append(", id=").append(id)
				.append(", price=").append(price).append(", publishedDate=").append(publishedDate).append(", title=")
				.append(title).append("]");
		return builder.toString();
	}

}
