package com.tawabocah.hibernate.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;

/**
 * For workbook generator & existing
 * 
 * @author Timotius Pamungkas
 *
 */
@Data
@Entity(name = "po_wb_generator_dtl")
public class WorkbookGeneratorOrderDtl implements Serializable {

	private static final long serialVersionUID = 3101809384814397054L;
	@Column(name = "allow_zero_or_negative")
	private boolean allowZeroOrNegative;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "creation_date", nullable = false)
	private Date creationDate;

	@Column(name = "last_update_date", nullable = false)
	private Date lastUpdateDate;

	@Column(name = "last_updated_by")
	private int lastUpdatedBy;

	@Column(name = "max_value", length = 6, nullable = false)
	private int maxValue;

	@Column(name = "min_value", length = 6, nullable = false)
	private int minValue;

	@Column(name = "page_size", nullable = false, length = 4)
	private int pageSize;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_wb_generator_hdr_id", nullable = false)
	private PurchaseOrderHdr poHeader;

	@Id
	@Column(name = "po_wb_generator_dtl_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "po_wb_generator_dtl_s")
	@SequenceGenerator(name = "po_wb_generator_dtl_s", sequenceName = "po_wb_generator_dtl_s", allocationSize = 1, initialValue = 1)
	private long poWorkbookGeneratorDetailId;

	@Column(name = "price_base", length = 7)
	private int priceBase;

	@Column(name = "title", length = 50)
	private String title;

	// ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION
	@Column(name = "workbook_type", length = 50, nullable = false)
	private String workbookType;

}
