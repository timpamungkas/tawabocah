package com.tawabocah.hibernate.dao;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tawabocah.hibernate.domain.PurchaseOrderHdr;
import com.tawabocah.spring.repository.PurchaseOrderRepository;

@Repository
@Scope("prototype")
public class PurchaseOrderDAO {

	@Autowired
	private PurchaseOrderRepository purchaseOrderRepository;
//	@Autowired
//	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return purchaseOrderRepository.count();
	}

	@Transactional
	public void delete(Iterable<PurchaseOrderHdr> purchaseOrders) {
		purchaseOrderRepository.delete(purchaseOrders);
	}

	@Transactional
	public void delete(Long id) {
		purchaseOrderRepository.delete(id);
	}

	@Transactional
	public void delete(PurchaseOrderHdr purchaseOrder) {
		purchaseOrderRepository.delete(purchaseOrder);
	}

	@Transactional
	public boolean exists(Long id) {
		return purchaseOrderRepository.exists(id);
	}

	@Transactional
	public Iterable<PurchaseOrderHdr> findAll() {
		return purchaseOrderRepository.findAll();
	}

	@Transactional
	public Iterable<PurchaseOrderHdr> findAll(Iterable<Long> ids) {
		return purchaseOrderRepository.findAll(ids);
	}

	@Transactional
	public Page<PurchaseOrderHdr> findAll(Pageable pageable) {
		return purchaseOrderRepository.findAll(pageable);
	}

	@Transactional
	public PurchaseOrderHdr findOne(Long id) {
		return purchaseOrderRepository.findOne(id);
	}

	@Transactional
	public Iterable<PurchaseOrderHdr> save(Iterable<PurchaseOrderHdr> purchaseOrders) {
		return purchaseOrderRepository.save(purchaseOrders);
	}

	@Transactional
	public PurchaseOrderHdr save(PurchaseOrderHdr purchaseOrder) {
		return purchaseOrderRepository.save(purchaseOrder);
	}

}
