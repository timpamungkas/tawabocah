package com.tawabocah.hibernate.dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.hibernate.domain.WorkbookCatalogItem;
import com.tawabocah.spring.repository.WorkbookCatalogItemRepository;

@Service
public class WorkbookCatalogItemDAO {

	@Autowired
	private WorkbookCatalogItemRepository workbookCatalogItemRepository;

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public long count() {
		return workbookCatalogItemRepository.count();
	}

	@Transactional
	public long count(Date publishedDateFrom, Date publishedDateTo, Collection<String> tags) throws SQLException {
		long result = 0l;
		try (Session session = sessionFactory.openSession()) {
			Criteria c;

			c = session.createCriteria(WorkbookCatalogItem.class).setProjection(Projections.count("id"));

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
					metaKeywordRestrictions[i++] = cTag;
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			result = (long) c.uniqueResult();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error count activities : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}

		return result;
	}

	@Transactional
	public void delete(Iterable<WorkbookCatalogItem> WorkbookCatalogItems) {
		workbookCatalogItemRepository.delete(WorkbookCatalogItems);
	}

	@Transactional
	public void delete(Long id) {
		workbookCatalogItemRepository.delete(id);
	}

	@Transactional
	public void delete(WorkbookCatalogItem WorkbookCatalogItem) {
		workbookCatalogItemRepository.delete(WorkbookCatalogItem);
	}

	@Transactional
	public boolean exists(Long id) {
		return workbookCatalogItemRepository.exists(id);
	}

	/**
	 * Find item by asset path
	 * 
	 * @param assetPath
	 * @return false if assetPath is empty, check to database otherwise
	 */
	@Transactional
	public boolean exists(String assetPath) {
		Session session = sessionFactory.openSession();
		Criteria c = session.createCriteria(WorkbookCatalogItem.class);

		if (!StringUtils.isEmpty(assetPath)) {
			c.add(Restrictions.eq("assetPath", assetPath));
		} else {
			return false;
		}

		WorkbookCatalogItem itemResult = (WorkbookCatalogItem) c.uniqueResult();

		return itemResult == null ? false : true;
	}

	@Transactional
	public Iterable<WorkbookCatalogItem> findAll() {
		return workbookCatalogItemRepository.findAll();
	}

	@Transactional
	public Iterable<WorkbookCatalogItem> findAll(Iterable<Long> ids) {
		return workbookCatalogItemRepository.findAll(ids);
	}

	@Transactional
	public Page<WorkbookCatalogItem> findAll(Pageable pageable) {
		return workbookCatalogItemRepository.findAll(pageable);
	}

	@Transactional
	public WorkbookCatalogItem findOne(Long id) {
		return workbookCatalogItemRepository.findOne(id);
	}

	/**
	 * Find specified worksheet. Avoid fetching html content if necessary (e.g.
	 * for index/inquiry page). Result from such method can be used to populate
	 * thumbnails.
	 * 
	 * @param fetchHtmlContent
	 *            is true will fetch html content to {@link WorkbookCatalogItem}
	 *            . Proceed with cautions, fetching html means fetching full
	 *            html source (CLOB).
	 * @param fetchHtmlContent
	 *            if true will fetch clob for html content. Use false if such
	 *            content not required to be rendered
	 * @param publishedDateFrom
	 * @param publishedDateTo
	 * @param tags
	 *            collection of tag
	 * @param startIndex
	 *            hibernate start index, zero based. If < 0 will use 0 instead
	 * @param limit
	 *            number of items retrieved. If 0 will get all items
	 * @param random
	 *            if true will get random element (NOTE using mysql specific
	 *            query)
	 * @param orders
	 *            Hibernate order, results will be ordered accoridng sequences
	 *            of this varargs
	 * @return list of post item
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<WorkbookCatalogItem> findAll(boolean fetchHtmlContent, Date publishedDateFrom, Date publishedDateTo,
			Collection<String> tags, int startIndex, int limit, boolean random, Order... orders) throws SQLException {
		List<WorkbookCatalogItem> resultFetch = null;
		List<WorkbookCatalogItem> result = null;

		try (Session session = sessionFactory.openSession()) {
			// select the ID
			Criteria c = session.createCriteria(WorkbookCatalogItem.class)
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
							.add(Projections.property("title"), "title")
							.add(Projections.property("publishedDate"), "publishedDate")
							.add(Projections.property("thumbnailPath"), "thumbnailPath")
							.add(Projections.property("price"), "price")

					).setResultTransformer(Transformers.aliasToBean(WorkbookCatalogItem.class));

			if (publishedDateFrom != null || publishedDateTo != null) {
				if (publishedDateFrom == null) {
					Calendar calFrom = Calendar.getInstance();
					calFrom.set(2015, 0, 1);
					publishedDateFrom = calFrom.getTime();
				}

				if (publishedDateTo == null) {
					publishedDateTo = Calendar.getInstance().getTime();
				}

				c.add(Restrictions.between("publishedDate", publishedDateFrom, publishedDateTo));
			}

			if (tags != null && !tags.isEmpty()) {
				Criterion[] metaKeywordRestrictions = new Criterion[tags.size()];
				int i = 0;
				for (String tag : tags) {
					if (!StringUtils.isEmpty(tag)) {
						Criterion cTag = Restrictions.ilike("metaKeywords", tag, MatchMode.ANYWHERE);
						metaKeywordRestrictions[i++] = cTag;
					}
				}

				Disjunction metaKeywordDisjunction = Restrictions.disjunction(metaKeywordRestrictions);
				c.add(metaKeywordDisjunction);
			}

			if (orders.length == 0) {
				c.addOrder(Order.desc("publishedDate"));
			} else {
				for (Order order : orders) {
					c.addOrder(order);
				}
			}

			if (startIndex < 0) {
				startIndex = 0;
			}

			if (limit > 0) {
				resultFetch = c.setFirstResult(startIndex).setMaxResults(limit).list();
			} else {
				resultFetch = c.list();
			}

			if (resultFetch == null || resultFetch.isEmpty()) {
				result = Lists.newArrayList();
			} else {
				result = resultFetch;

				if (random) {
					Collections.shuffle(result);
				}
			}

			return result;
		} catch (Exception ex) {
			String errorMessage = new StringBuilder().append("Error find catalog item : ").append(ex.getMessage())
					.toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public Iterable<WorkbookCatalogItem> save(Iterable<WorkbookCatalogItem> WorkbookCatalogItems) {
		return workbookCatalogItemRepository.save(WorkbookCatalogItems);
	}

	@Transactional
	public WorkbookCatalogItem save(WorkbookCatalogItem WorkbookCatalogItem) {
		return workbookCatalogItemRepository.save(WorkbookCatalogItem);
	}

	@Transactional
	public void saveOrUpdate(WorkbookCatalogItem workbookCatalogItem) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(workbookCatalogItem);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error save/update catalog item: ")
					.append(workbookCatalogItem.getAssetPath()).append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

	@Transactional
	public void update(WorkbookCatalogItem WorkbookCatalogItem) throws SQLException {
		try (Session session = sessionFactory.openSession()) {
			Transaction t = session.beginTransaction();
			session.update(WorkbookCatalogItem);
			t.commit();
		} catch (Exception ex) {
			String errorMessage = new StringBuilder("Error update catalog item: ")
					.append(WorkbookCatalogItem.getAssetPath()).append(" : ").append(ex.getMessage()).toString();
			throw new SQLException(errorMessage);
		}
	}

}
