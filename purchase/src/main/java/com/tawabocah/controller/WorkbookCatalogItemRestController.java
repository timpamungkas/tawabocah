package com.tawabocah.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tawabocah.constants.PagingAndSortingConstants;
import com.tawabocah.hibernate.domain.WorkbookCatalogItem;
import com.tawabocah.service.WorkbookCatalogItemService;

@RestController
@RequestMapping(path = "/api/v1")
public class WorkbookCatalogItemRestController {

	@Autowired
	private WorkbookCatalogItemService workbookCatalogItemServiceImpl;
	
	@RequestMapping(path="/") 
	public String test() {
		return "test";
	}

	@RequestMapping(path = "/workbook/catalog/{id}", method = RequestMethod.GET)
	public WorkbookCatalogItem getById(@PathVariable Long id) {
		return workbookCatalogItemServiceImpl.findOne(id);
	}

	@RequestMapping(path = "/workbook/catalog/published/count", method = RequestMethod.GET)
	public long findPublishedPageableCount() {
		Date publishedDateTo = Calendar.getInstance().getTime();
		return workbookCatalogItemServiceImpl.count(null, publishedDateTo, null);
	}

	@RequestMapping(path = "/workbook/catalog/published/list", method = RequestMethod.GET)
	public List<WorkbookCatalogItem> findPublishedPageable(
			@RequestParam(value = "ps", required = false) Integer pageSize,
			@RequestParam(value = "p", required = false) Integer page,
			@RequestParam(value = "sb", required = false) String sortBy,
			@RequestParam(value = "g", required = false) String grade) {
		String evalSortBy = StringUtils.isEmpty(sortBy) ? PagingAndSortingConstants.INITIAL_SORT_BY : sortBy;
		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize == null ? PagingAndSortingConstants.INITIAL_PAGE_SIZE : pageSize;
		boolean validPageSize = false;
		for (int allowedPageSize : PagingAndSortingConstants.PAGE_SIZES) {
			if (evalPageSize == allowedPageSize) {
				validPageSize = true;
				break;
			}
		}
		if (!validPageSize) {
			evalPageSize = PagingAndSortingConstants.INITIAL_PAGE_SIZE;
		}

		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page == null || page < 1) ? PagingAndSortingConstants.INITIAL_PAGE : page;

		return workbookCatalogItemServiceImpl.findPublishedPageable(evalPage, evalPageSize, evalSortBy, grade);
	}

	@RequestMapping(path = "/workbook/post/save", method = RequestMethod.POST)
	public long save(@RequestBody WorkbookCatalogItem workbookCatalogItem) {
		Date now = Calendar.getInstance().getTime();
		
		workbookCatalogItem.setCreationDate(now);
		workbookCatalogItem.setLastUpdateDate(now);
		workbookCatalogItem.setPublishedDate(now);
		
		WorkbookCatalogItem savedPost = workbookCatalogItemServiceImpl.save(workbookCatalogItem);

		return savedPost == null ? 0 : savedPost.getId();
	}
	
}
