package com.tawabocah.service;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public interface PDFWorkbookService {

	int FONT_SIZE = 20;
	PDFont FONT_TYPE = PDType1Font.COURIER;
	int LEFT_PAD = 4;
	/**
	 * Number of problem per page (affect PDF layouting)
	 */
	byte N_PROBLEM_EACH_PAGE = 15;
	/**
	 * Number of problem per row
	 */
	byte N_PROBLEM_EACH_ROW = 3;
	PDRectangle PAGE_SIZE = PDRectangle.A4;
	String REGEX_REMOVE_TRAILING_SPACES = "\\s+$";
	/**
	 * Height per line
	 */
	float ROW_HEIGHT = 22f;
	String SEPARATOR = "       ";
	String FOOTER_TEXT = "tawabocah.com";
	PDFont FOOTER_FONT_TYPE = PDType1Font.HELVETICA;
	int FOOTER_FONT_SIZE = 10;
	float FOOTER_TEXT_Y = 25f;
	float X_SPACE_BETWEEN_PROBLEM = 131f;
	float LINE_STROKE_WIDTH = 51f;
	/**
	 * Left (all line) coordinate X, from PDFUtil.getCenteredX
	 */
	float X_START = 140;
	/**
	 * Top (1st line) coordinate Y
	 */
	float Y_START = 740f;
	
}
