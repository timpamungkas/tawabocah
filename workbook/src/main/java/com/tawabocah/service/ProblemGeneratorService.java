package com.tawabocah.service;

public interface ProblemGeneratorService {

	public enum SortBy {
		ASC, DESC, RANDOM;
	}

	public enum ProblemType {
		ADDITION("+"), SUBTRACTION("-"), MULTIPLICATION("x"), DIVISION("/");

		private String operator;

		private ProblemType(String operator) {
			this.operator = operator;
		}

		public String getOperator() {
			return operator;
		}
	}

}
