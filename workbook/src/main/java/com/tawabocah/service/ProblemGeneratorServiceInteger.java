package com.tawabocah.service;

import java.util.List;

import com.tawabocah.domain.ProblemInteger;

public interface ProblemGeneratorServiceInteger extends ProblemGeneratorService {

	/**
	 * Generate problem.
	 * 
	 * @param numOfProblems
	 *            num of worksheet item generated
	 * @param minValue
	 *            operand min value, must be <= maxValue
	 * @param maxValue
	 *            operand max value
	 * @param sortBy
	 *            sort by 1st operand, then 2nd operand
	 * @return
	 */
	List<ProblemInteger> generateAdditionProblems(int numOfProblems, Integer minValue, Integer maxValue, SortBy sortBy);

	List<ProblemInteger> generateSubtractionProblems(int numOfProblems, Integer minValue, Integer maxValue,
			boolean allowZeroOrNegative, SortBy sortBy);

	List<ProblemInteger> generateMultiplicationProblems(int numOfProblems, Integer minValue, Integer maxValue,
			SortBy sortBy);

	List<ProblemInteger> generateDivisionProblems(int numOfProblems, Integer minValue, Integer maxValue,
			boolean allowDecimalPoint, SortBy sortBy);

}
