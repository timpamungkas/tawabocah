package com.tawabocah.service.impl;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tawabocah.domain.ProblemInteger;
import com.tawabocah.pdf.PDFWorkbookCreator;
import com.tawabocah.pdf.WorkbookInteger;
import com.tawabocah.pdf.WorksheetInteger;
import com.tawabocah.service.PDFWorkbookServiceInteger;
import com.tawabocah.service.ProblemGeneratorService.SortBy;
import com.tawabocah.service.ProblemGeneratorServiceInteger;
import com.tawabocah.util.PDFUtil;

import lombok.extern.log4j.Log4j2;

@Service
@Scope("prototype")
@Log4j2
public class PDFWorkbookServiceIntegerImpl implements PDFWorkbookServiceInteger {

	@Autowired
	private PDFWorkbookCreator pdfWorkbookCreator;

	@Autowired
	private ProblemGeneratorServiceInteger problemGeneratorServiceInteger;

	private List<String> convertToListLines(List<ProblemInteger> listProblems) {
		// create list of string, each string is a line to be written
		int i = 0;
		StringBuilder sb1 = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		StringBuilder sb3 = new StringBuilder();
		StringBuilder sb4 = new StringBuilder();
		StringBuilder sb5 = new StringBuilder();
		List<String> listLines = Lists.newArrayListWithExpectedSize((listProblems.size() / N_PROBLEM_EACH_ROW) + 1);

		for (ProblemInteger problem : listProblems) {
			// 1 : 1st operand
			// 2 : 2nd operand
			// 3 : operator
			// 4 : result
			// 5 : vertical space

			sb1.append(StringUtils.leftPad(Integer.toString(problem.getNumberFirst()), LEFT_PAD, StringUtils.EMPTY));
			sb1.append(SEPARATOR);
			sb2.append(StringUtils.leftPad(Integer.toString(problem.getNumberSecond()), LEFT_PAD, StringUtils.EMPTY));
			sb2.append(SEPARATOR);
			sb3.append(StringUtils.leftPad(problem.getOperator(), LEFT_PAD, StringUtils.EMPTY));
			sb3.append(SEPARATOR);
			sb4.append(StringUtils.leftPad(Integer.toString(problem.getResult()), LEFT_PAD, StringUtils.EMPTY));
			sb4.append(SEPARATOR);
			sb5.append(StringUtils.EMPTY);

			i++;
			if (i == N_PROBLEM_EACH_ROW) { // reset new set of problems
				i = 0;
				listLines.add(sb1.toString().replaceAll(REGEX_REMOVE_TRAILING_SPACES, StringUtils.EMPTY));
				listLines.add(sb2.toString().replaceAll(REGEX_REMOVE_TRAILING_SPACES, StringUtils.EMPTY));
				listLines.add(sb3.toString().replaceAll(REGEX_REMOVE_TRAILING_SPACES, StringUtils.EMPTY));
				listLines.add(sb4.toString().replaceAll(REGEX_REMOVE_TRAILING_SPACES, StringUtils.EMPTY));
				listLines.add(sb5.toString().replaceAll(REGEX_REMOVE_TRAILING_SPACES, StringUtils.EMPTY));
				sb1.delete(0, sb1.length());
				sb2.delete(0, sb2.length());
				sb3.delete(0, sb3.length());
				sb4.delete(0, sb4.length());
				sb5.delete(0, sb5.length());
			}
		}

		return listLines;
	}

	@Override
	public WorkbookInteger createAdditionWorkbook(int pageSize, Integer minValue, Integer maxValue, SortBy sortBy) {
		WorkbookInteger workbook = new WorkbookInteger();
		int numOfProblems = pageSize * N_PROBLEM_EACH_PAGE;
		List<ProblemInteger> listProblems = problemGeneratorServiceInteger.generateAdditionProblems(numOfProblems,
				minValue, maxValue, sortBy);

		// create pages
		List<List<ProblemInteger>> listProblemsPerWorksheet = Lists.partition(listProblems, N_PROBLEM_EACH_PAGE);

		listProblemsPerWorksheet.forEach(l -> {
			WorksheetInteger page = new WorksheetInteger(l);
			workbook.addWorksheet(page);
		});

		return workbook;
	}

	@Override
	public WorkbookInteger createMultiplicationWorkbook(int pageSize, Integer minValue, Integer maxValue,
			SortBy sortBy) {
		WorkbookInteger workbook = new WorkbookInteger();
		int numOfProblems = pageSize * N_PROBLEM_EACH_PAGE;
		List<ProblemInteger> listProblems = problemGeneratorServiceInteger.generateMultiplicationProblems(numOfProblems,
				minValue, maxValue, sortBy);

		// create pages
		List<List<ProblemInteger>> listProblemsPerWorksheet = Lists.partition(listProblems, N_PROBLEM_EACH_PAGE);

		listProblemsPerWorksheet.forEach(l -> {
			WorksheetInteger page = new WorksheetInteger(l);
			workbook.addWorksheet(page);
		});

		return workbook;
	}

	/**
	 * 
	 * @param document
	 * @param page
	 *            if null, will create new page
	 * @param listProblems
	 * @param showResult
	 * @return
	 */
	private PDPage createPDPage(PDDocument document, PDPage page, List<ProblemInteger> listProblems, boolean showResult,
			String contentTitle, Color contentTitleColor) {
		List<String> listLines = convertToListLines(listProblems);

		if (page == null) {
			page = new PDPage(PAGE_SIZE);
		}

		try {
			PDPageContentStream contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true);
			contentStream.setFont(FONT_TYPE, FONT_SIZE);

			float y = Y_START;
			int lineNum = 1;
			for (String line : listLines) {
				if (lineNum % 5 == 3) {
					contentStream.setNonStrokingColor(Color.BLACK);
					contentStream.setStrokingColor(Color.BLACK);

					// operator
					float y1 = y + (ROW_HEIGHT / 2);
					// adjust for operator string ( + + +)
					y += (ROW_HEIGHT / 4);

					contentStream.beginText();
					contentStream.newLineAtOffset(X_START + 18, y);
					contentStream.showText(line);
					contentStream.endText();

					// lines, better write line manually to better-look on
					// top/bottom padding
					for (int j = 0; j < 3; j++) {
						float x1 = X_START + (j * X_SPACE_BETWEEN_PROBLEM) + LINE_STROKE_WIDTH;
						contentStream.moveTo(X_START + (j * X_SPACE_BETWEEN_PROBLEM), y1);
						contentStream.lineTo(x1, y1);
						contentStream.stroke();
					}
				} else if (lineNum % 5 == 4) {
					// result
					if (showResult) {
						contentStream.setNonStrokingColor(Color.RED);

						contentStream.beginText();
						contentStream.newLineAtOffset(X_START, y);
						contentStream.showText(line);
						contentStream.endText();
					}
				} else if (lineNum % 5 == 0) {
					// vertical space
					y -= ROW_HEIGHT * 1.75;
				} else {
					contentStream.setNonStrokingColor(Color.BLACK);

					contentStream.beginText();
					contentStream.newLineAtOffset(X_START, y);
					contentStream.showText(line);
					contentStream.endText();
				}
				y -= ROW_HEIGHT;
				lineNum++;
			}

			// footer
			if (contentTitleColor == null) {
				contentStream.setNonStrokingColor(Color.BLACK);
			} else {
				contentStream.setNonStrokingColor(contentTitleColor);
			}
			PDFUtil.addCenteredTextHorizontally(FOOTER_TEXT, FOOTER_FONT_TYPE, FOOTER_FONT_SIZE, contentStream, page,
					FOOTER_TEXT_Y);

			// upper title
			contentStream.setFont(PDType1Font.HELVETICA, 20);
			contentStream.beginText();
			contentStream.newLineAtOffset(330, 792);
			contentStream.showText(contentTitle);
			contentStream.endText();

			// Save the results and ensure that the document is properly closed:
			contentStream.close();
		} catch (IOException e) {
			log.error("Can't create page : " + e.getMessage());
		}

		return page;
	}

	@Override
	public WorkbookInteger createSubtractionWorkbook(int pageSize, Integer minValue, Integer maxValue,
			boolean allowZeroOrNegative, SortBy sortBy) {
		WorkbookInteger workbook = new WorkbookInteger();
		int numOfProblems = pageSize * N_PROBLEM_EACH_PAGE;
		List<ProblemInteger> listProblems = problemGeneratorServiceInteger.generateSubtractionProblems(numOfProblems,
				minValue, maxValue, allowZeroOrNegative, sortBy);

		// create pages
		List<List<ProblemInteger>> listProblemsPerWorksheet = Lists.partition(listProblems, N_PROBLEM_EACH_PAGE);

		listProblemsPerWorksheet.forEach(l -> {
			WorksheetInteger page = new WorksheetInteger(l);
			workbook.addWorksheet(page);
		});

		return workbook;
	}

	public static final Map<String, Color> mapColor = Maps.newHashMapWithExpectedSize(4);

	static {
		mapColor.put("01", new Color(198, 40, 40)); // red 800
		mapColor.put("02", new Color(40, 53, 147)); // blue 800
		mapColor.put("03", new Color(46, 125, 50)); // green 800
		mapColor.put("04", new Color(78, 52, 46)); // brown 800
	}

	@Override
	public void saveWorkbook(WorkbookInteger workbook, File target, String title, String contentTitle) {
		String setNumber = StringUtils.leftPad(Integer.toString(ThreadLocalRandom.current().nextInt(1, 5)), 2, '0');
		URL urlCoverPage = PDFWorkbookServiceIntegerImpl.class.getClassLoader()
				.getResource("template/Cover" + setNumber + ".pdf");
		URL urlLicensePage = PDFWorkbookServiceIntegerImpl.class.getClassLoader()
				.getResource("template/License" + setNumber + ".pdf");
		URL urlContentPage = PDFWorkbookServiceIntegerImpl.class.getClassLoader()
				.getResource("template/Content" + setNumber + ".pdf");
		URL urlCoverAnswerPage = PDFWorkbookServiceIntegerImpl.class.getClassLoader()
				.getResource("template/CoverAnswer" + setNumber + ".pdf");

		Color color = mapColor.get(setNumber);
		PDDocument document = new PDDocument();
		PDDocument docCoverPage = null;
		PDDocument docLicensePage = null;
		PDDocument docCoverAnswerPage = null;
		PDPage coverPage = null;
		PDPage licensePage = null;
		PDPage coverAnswerPage = null;
		PDPage contentPageProblem = null;
		PDPage contentPageAnswer = null;

		try {
			if (urlCoverPage != null) {
				docCoverPage = PDDocument.load(urlCoverPage.openStream());
				coverPage = docCoverPage.getPage(0);

				addWorkbookTitleRightAlign(docCoverPage, coverPage, title, PDType1Font.HELVETICA, 32, Color.WHITE);
			} else {
				coverPage = PDFUtil.createCoverPage(document);
			}

			if (urlLicensePage != null) {
				docLicensePage = PDDocument.load(urlLicensePage.openStream());
				licensePage = docLicensePage.getPage(0);
			}

			if (urlCoverAnswerPage != null) {
				docCoverAnswerPage = PDDocument.load(urlCoverAnswerPage.openStream());
				coverAnswerPage = docCoverAnswerPage.getPage(0);
			}

			List<PDPage> problemPages = Lists.newArrayListWithExpectedSize(workbook.getWorksheets().size());
			List<PDPage> answerPages = Lists.newArrayListWithExpectedSize(workbook.getWorksheets().size());
			List<PDDocument> tempDocs = Lists.newArrayList();

			for (WorksheetInteger worksheet : workbook.getWorksheets()) {
				PDDocument docContentPageProblem = null;
				PDDocument docContentPageAnswer = null;
				if (urlContentPage != null) {
					docContentPageProblem = PDDocument.load(urlContentPage.openStream());
					contentPageProblem = docContentPageProblem.getPage(0);
					tempDocs.add(docContentPageProblem);

					docContentPageAnswer = PDDocument.load(urlContentPage.openStream());
					contentPageAnswer = docContentPageAnswer.getPage(0);
					tempDocs.add(docContentPageAnswer);
				}

				PDPage problemPage = createPDPage(docContentPageProblem, contentPageProblem,
						worksheet.getListProblems(), false, contentTitle, color);
				PDPage answerPage = createPDPage(docContentPageAnswer, contentPageAnswer, worksheet.getListProblems(),
						true, contentTitle, color);
				problemPages.add(problemPage);
				answerPages.add(answerPage);
			}

			pdfWorkbookCreator.create(target, coverPage, coverAnswerPage, licensePage, problemPages, answerPages, title,
					"tawabocah.com", StringUtils.EMPTY, "buku latihan anak, "
							+ "lembar latihan matematika, penjumlahan, " + "pengurangan anak usia PAUD/TK/SD");

			if (document != null) {
				document.close();
			}

			if (docCoverAnswerPage != null) {
				docCoverAnswerPage.close();
			}
			if (docCoverPage != null) {
				docCoverPage.close();
			}
			if (docLicensePage != null) {
				docLicensePage.close();
			}

			for (PDDocument docContent : tempDocs) {
				docContent.close();
			}
		} catch (IOException | TransformerException e) {
			log.error("Can't save workbook : " + e.getMessage());
		}
	}

	private void addWorkbookTitleRightAlign(PDDocument docCoverPage, PDPage coverPage, String title, PDFont fontType,
			int fontSize, Color fontColor) throws IOException {
		PDPageContentStream contentStream = new PDPageContentStream(docCoverPage, coverPage, AppendMode.APPEND, true);
		boolean needsMultiline = true;
		float margin = 20f;
		float startX = margin; // on left margin
		while (needsMultiline) {
			float stringWidth = PDFUtil.getStringWidth(title, fontType, fontSize);

			if (stringWidth < coverPage.getMediaBox().getWidth() - (2 * margin)) {
				needsMultiline = false;
				startX = coverPage.getMediaBox().getWidth() - (2 * margin) - stringWidth;
			}
			fontSize -= 1;
		}

		contentStream.setFont(PDType1Font.HELVETICA, fontSize);
		contentStream.setNonStrokingColor(fontColor);
		contentStream.beginText();
		contentStream.newLineAtOffset(startX, 685);
		contentStream.showText(title);
		contentStream.endText();
		contentStream.close();
	}

	@Override
	public WorkbookInteger createDivisionWorkbook(int pageSize, Integer minValue, Integer maxValue,
			boolean allowDecimal, SortBy sortBy) {
		WorkbookInteger workbook = new WorkbookInteger();
		int numOfProblems = pageSize * N_PROBLEM_EACH_PAGE;
		List<ProblemInteger> listProblems = problemGeneratorServiceInteger.generateSubtractionProblems(numOfProblems,
				minValue, maxValue, allowDecimal, sortBy);

		// create pages
		List<List<ProblemInteger>> listProblemsPerWorksheet = Lists.partition(listProblems, N_PROBLEM_EACH_PAGE);

		listProblemsPerWorksheet.forEach(l -> {
			WorksheetInteger page = new WorksheetInteger(l);
			workbook.addWorksheet(page);
		});

		return workbook;
	}

}
