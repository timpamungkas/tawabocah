package com.tawabocah.service.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tawabocah.domain.ProblemAdditionInteger;
import com.tawabocah.domain.ProblemDivisionInteger;
import com.tawabocah.domain.ProblemInteger;
import com.tawabocah.domain.ProblemMultiplicationInteger;
import com.tawabocah.domain.ProblemSubtractionInteger;
import com.tawabocah.service.ProblemGeneratorServiceInteger;

@Service
@Scope("prototype")
public class ProblemGeneratorServiceIntegerImpl implements ProblemGeneratorServiceInteger {

	@Override
	/**
	 * Duplicate problem allowed if number of requested problems > possible
	 * combinations.
	 */
	public List<ProblemInteger> generateAdditionProblems(int numOfProblems, Integer minValue, Integer maxValue,
			SortBy sortBy) {
		if (minValue > maxValue) {
			throw new IllegalArgumentException("minValue must be <= maxValue");
		}
		boolean isAllowDuplicate = false;

		int nElements = maxValue - minValue + 1;
		int possibleCombinations = nElements * nElements;

		if (numOfProblems > possibleCombinations) {
			isAllowDuplicate = true;
		}

		List<ProblemInteger> result = Lists.newArrayList();

		while (result.size() < numOfProblems) {
			ProblemInteger elmt = new ProblemAdditionInteger(ProblemType.ADDITION.getOperator(), minValue, maxValue);

			if (isAllowDuplicate) { // check for duplicate
				result.add(elmt);
			} else { // just add it
				if (!result.contains(elmt)) {
					result.add(elmt);
				}
			}
		}

		switch (sortBy) {
		case ASC:
			Collections.sort(result);
			break;
		case DESC:
			Collections.sort(result, Collections.reverseOrder());
			break;
		case RANDOM:
			Collections.shuffle(result);
			break;
		}

		return result;
	}

	@Override
	public List<ProblemInteger> generateSubtractionProblems(int numOfProblems, Integer minValue, Integer maxValue,
			boolean allowZeroOrNegative, SortBy sortBy) {
		if (minValue > maxValue) {
			throw new IllegalArgumentException("minValue must be <= maxValue");
		}
		boolean isAllowDuplicate = false;

		int nElements = maxValue - minValue + 1;
		int possibleCombinations = nElements * nElements;

		if (numOfProblems > possibleCombinations) {
			isAllowDuplicate = true;
		}

		List<ProblemInteger> result = Lists.newArrayList();

		while (result.size() < numOfProblems) {
			ProblemInteger elmt = new ProblemSubtractionInteger(ProblemType.SUBTRACTION.getOperator(), minValue,
					maxValue, allowZeroOrNegative);

			if (isAllowDuplicate) { // check for duplicate
				result.add(elmt);
			} else { // just add it
				if (!result.contains(elmt)) {
					result.add(elmt);
				}
			}
		}

		switch (sortBy) {
		case ASC:
			Collections.sort(result);
			break;
		case DESC:
			Collections.sort(result, Collections.reverseOrder());
			break;
		case RANDOM:
			Collections.shuffle(result);
			break;
		}

		return result;
	}

	@Override
	public List<ProblemInteger> generateMultiplicationProblems(int numOfProblems, Integer minValue, Integer maxValue,
			SortBy sortBy) {
		if (minValue > maxValue) {
			throw new IllegalArgumentException("minValue must be <= maxValue");
		}
		boolean isAllowDuplicate = false;

		int nElements = maxValue - minValue + 1;
		int possibleCombinations = nElements * nElements;

		if (numOfProblems > possibleCombinations) {
			isAllowDuplicate = true;
		}

		List<ProblemInteger> result = Lists.newArrayList();

		while (result.size() < numOfProblems) {
			ProblemInteger elmt = new ProblemMultiplicationInteger(ProblemType.MULTIPLICATION.getOperator(), minValue,
					maxValue);

			if (isAllowDuplicate) { // check for duplicate
				result.add(elmt);
			} else { // just add it
				if (!result.contains(elmt)) {
					result.add(elmt);
				}
			}
		}

		switch (sortBy) {
		case ASC:
			Collections.sort(result);
			break;
		case DESC:
			Collections.sort(result, Collections.reverseOrder());
			break;
		case RANDOM:
			Collections.shuffle(result);
			break;
		}

		return result;
	}

	@Override
	public List<ProblemInteger> generateDivisionProblems(int numOfProblems, Integer minValue, Integer maxValue,
			boolean allowDecimalPoint, SortBy sortBy) {
		if (minValue > maxValue) {
			throw new IllegalArgumentException("minValue must be <= maxValue");
		}
		boolean isAllowDuplicate = false;

		int nElements = maxValue - minValue + 1;
		int possibleCombinations = nElements * nElements;

		if (numOfProblems > possibleCombinations) {
			isAllowDuplicate = true;
		}

		List<ProblemInteger> result = Lists.newArrayList();

		while (result.size() < numOfProblems) {
			ProblemInteger elmt = new ProblemDivisionInteger(ProblemType.DIVISION.getOperator(), minValue, maxValue,
					false);

			if (isAllowDuplicate) { // check for duplicate
				result.add(elmt);
			} else { // just add it
				if (!result.contains(elmt)) {
					result.add(elmt);
				}
			}
		}

		switch (sortBy) {
		case ASC:
			Collections.sort(result);
			break;
		case DESC:
			Collections.sort(result, Collections.reverseOrder());
			break;
		case RANDOM:
			Collections.shuffle(result);
			break;
		}

		return result;
	}

}
