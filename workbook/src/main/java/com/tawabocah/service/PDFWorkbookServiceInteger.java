package com.tawabocah.service;

import java.io.File;

import com.tawabocah.pdf.WorkbookInteger;
import com.tawabocah.service.ProblemGeneratorService.SortBy;

public interface PDFWorkbookServiceInteger extends PDFWorkbookService {

	WorkbookInteger createAdditionWorkbook(int pageSize, Integer minValue, Integer maxValue, SortBy sortBy);

	WorkbookInteger createSubtractionWorkbook(int pageSize, Integer minValue, Integer maxValue,
			boolean allowZeroOrNegative, SortBy sortBy);

	void saveWorkbook(WorkbookInteger workbook, File target, String title, String contentTitle);

	WorkbookInteger createMultiplicationWorkbook(int pageSize, Integer minValue, Integer maxValue, SortBy sortBy);

	WorkbookInteger createDivisionWorkbook(int pageSize, Integer minValue, Integer maxValue, boolean allowDecimal,
			SortBy sortBy);

}
