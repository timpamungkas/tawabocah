package com.tawabocah.domain;

import lombok.Data;

/**
 * DTO for addition worksheet (1st number + 2nd number = result) Contains 2
 * operands & a result
 * 
 * @author Timotius Pamungkas
 *
 */
@Data
public abstract class ProblemInteger implements Comparable<ProblemInteger> {

	/**
	 * Man value for a operands
	 */
	private Integer maxValue;
	/**
	 * Min value for both operands
	 */
	private Integer minValue;
	private Integer numberFirst;
	private Integer numberSecond;
	private String operator;

	protected ProblemInteger() {

	}

	public ProblemInteger(String operator, Integer minValue, Integer maxValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.operator = operator;

		randomizeOperands();
	}

	public int compareTo(ProblemInteger o) {
		int byFirst = this.getNumberFirst().compareTo(o.getNumberFirst());
		return byFirst != 0 ? byFirst : this.getNumberSecond().compareTo(o.getNumberSecond());
	}

	public abstract Integer getResult();

	/**
	 * Randomize both operands based on minValue & maxValue as lower/upper
	 * treshold
	 */
	protected abstract void randomizeOperands();

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProblemInteger [maxValue=").append(maxValue).append(", minValue=").append(minValue)
				.append(", numberFirst=").append(numberFirst).append(", numberSecond=").append(numberSecond)
				.append(", operator=").append(operator).append("]");
		return builder.toString();
	}
}
