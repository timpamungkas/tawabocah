package com.tawabocah.domain;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

public class ProblemDivisionInteger extends ProblemInteger {

	@Getter
	@Setter
	private boolean allowDecimal = false;

	public ProblemDivisionInteger(String operator, Integer minValue, Integer maxValue, boolean allowDecimal) {
		this.setMinValue(minValue);
		this.setOperator(operator);
		this.setMaxValue(maxValue);
		this.setAllowDecimal(allowDecimal);

		this.randomizeOperands();
	}

	@Override
	public Integer getResult() {
		return getNumberFirst() / getNumberSecond();
	}

	@Override
	protected void randomizeOperands() {
		this.setNumberFirst(ThreadLocalRandom.current().nextInt(getMinValue(), getMaxValue() + 1));
		List<Integer> possibleSeeds = Lists.newArrayList();

		Integer seed = 0;
		if (allowDecimal) {
			// max 2 digit decimal
			int numberFirstMultiply100 = getNumberFirst() * 100;

			for (int i = 1; i <= getNumberFirst(); i++) {
				if (numberFirstMultiply100 % (i * 1) == 0) {
					possibleSeeds.add(i);
				}
			}
			seed = possibleSeeds.get(ThreadLocalRandom.current().nextInt(0, possibleSeeds.size()));
		} else {
			for (int i = 1; i <= getNumberFirst(); i++) {
				if (getNumberFirst() % i == 0) {
					possibleSeeds.add(i);
				}
			}
			seed = possibleSeeds.get(ThreadLocalRandom.current().nextInt(0, possibleSeeds.size()));
		}

		this.setNumberSecond(seed);
	}

}
