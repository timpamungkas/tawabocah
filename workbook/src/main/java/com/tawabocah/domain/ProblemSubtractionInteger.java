package com.tawabocah.domain;

import java.util.concurrent.ThreadLocalRandom;

import lombok.Getter;
import lombok.Setter;

public class ProblemSubtractionInteger extends ProblemInteger {

	@Getter
	@Setter
	private boolean allowZeroOrNegative = false;

	public ProblemSubtractionInteger(String operator, Integer minValue, Integer maxValue, boolean allowZeroOrNegative) {
		if (!allowZeroOrNegative && minValue == 0) {
			// overide, because if min value =0, there is possibility that first
			// number = 0. 0 - 0 will resulting 0, while this is not allow zero
			// or negative
			this.setMinValue(1);
		} else {
			this.setMinValue(minValue);
		}

		this.setOperator(operator);
		this.setMaxValue(maxValue);
		this.setAllowZeroOrNegative(allowZeroOrNegative);

		this.randomizeOperands();
	}

	@Override
	public Integer getResult() {
		return getNumberFirst() - getNumberSecond();
	}

	@Override
	protected void randomizeOperands() {
		this.setNumberFirst(ThreadLocalRandom.current().nextInt(getMinValue(), getMaxValue() + 1));

		Integer seed = 0;
		if (allowZeroOrNegative) {
			seed = ThreadLocalRandom.current().nextInt(getMinValue(), getMaxValue() + 1);
		} else {
			seed = ThreadLocalRandom.current().nextInt(getMinValue(), getNumberFirst() + 1);
		}

		this.setNumberSecond(seed);
	}

}
