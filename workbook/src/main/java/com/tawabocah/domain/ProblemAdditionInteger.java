package com.tawabocah.domain;

import java.util.concurrent.ThreadLocalRandom;

public class ProblemAdditionInteger extends ProblemInteger {

	public ProblemAdditionInteger(String operator, Integer minValue, Integer maxValue) {
		super(operator, minValue, maxValue);
	}

	@Override
	public Integer getResult() {
		return getNumberFirst() + getNumberSecond();
	}

	@Override
	protected void randomizeOperands() {
		this.setNumberFirst(ThreadLocalRandom.current().nextInt(getMinValue(), getMaxValue() + 1));
		this.setNumberSecond(ThreadLocalRandom.current().nextInt(getMinValue(), getMaxValue() + 1));
	}

}
