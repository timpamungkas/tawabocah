package com.tawabocah.util;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.apache.pdfbox.util.Matrix;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.schema.DublinCoreSchema;
import org.apache.xmpbox.schema.PDFAIdentificationSchema;
import org.apache.xmpbox.schema.XMPBasicSchema;
import org.apache.xmpbox.type.BadFieldValueException;
import org.apache.xmpbox.xml.XmpSerializer;

import com.tawabocah.service.PDFWorkbookService;

import lombok.extern.log4j.Log4j2;

/**
 * Helper for pdf framework, to create pdf or metadata
 * 
 * @author Timotius Pamungkas
 *
 */
@Log4j2
public class PDFUtil {

	// from
	// http://stackoverflow.com/questions/6507124/how-to-center-a-text-using-pdfbox
	/**
	 * Centered vertically & horizontally
	 * 
	 * @param text
	 * @param font
	 * @param fontSize
	 * @param content
	 * @param page
	 * @param offset
	 * @throws IOException
	 */
	public static void addCenteredText(String text, PDFont font, int fontSize, PDPageContentStream content, PDPage page,
			@NotNull Point2D.Float offset) throws IOException {
		content.setFont(font, fontSize);
		content.beginText();

		// Rotate the text according to the page orientation
		boolean pageIsLandscape = isLandscape(page);
		Point2D.Float pageCenter = getCenter(page);

		// We use the text's width to place it at the center of the page
		float stringWidth = getStringWidth(text, font, fontSize);
		if (pageIsLandscape) {
			float textX = pageCenter.x - stringWidth / 2F + offset.x;
			float textY = pageCenter.y - offset.y;
			// Swap X and Y due to the rotation
			content.setTextMatrix(Matrix.getRotateInstance(Math.PI / 2, textY, textX));
		} else {
			float textX = pageCenter.x - stringWidth / 2F + offset.x;
			float textY = pageCenter.y + offset.y;
			content.setTextMatrix(Matrix.getTranslateInstance(textX, textY));
		}

		content.showText(text);
		content.endText();
	}

	public static void addCenteredTextHorizontally(String text, PDFont font, int fontSize, PDPageContentStream content,
			PDPage page, float textY) throws IOException {
		content.setFont(font, fontSize);
		content.beginText();

		// Rotate the text according to the page orientation
		boolean pageIsLandscape = isLandscape(page);
		Point2D.Float pageCenter = getCenter(page);

		// We use the text's width to place it at the center of the page
		float stringWidth = getStringWidth(text, font, fontSize);
		if (pageIsLandscape) {
			float textX = pageCenter.x - stringWidth / 2F;
			content.setTextMatrix(Matrix.getRotateInstance(Math.PI / 2, textY, textX));
		} else {
			float textX = pageCenter.x - stringWidth / 2F;
			content.setTextMatrix(Matrix.getTranslateInstance(textX, textY));
		}

		content.showText(text);
		content.endText();
	}

	/**
	 * Create cover page, A4 size, portrait.
	 * 
	 * @param document
	 * @param title
	 * @return
	 */
	public static PDPage createCoverAnswerPage(PDDocument document, String title) {
		PDPage page = new PDPage(PDFWorkbookService.PAGE_SIZE);

		try {
			PDImageXObject pdImage = PDImageXObject.createFromFile("D:/FIF/image.jpg", document);
			PDPageContentStream contentStream = new PDPageContentStream(document, page);

			Point2D.Float pageCenter = PDFUtil.getCenter(page);

			float scale = 1f;
			float textX = pageCenter.x - pdImage.getWidth() / 2F;
			float textY = pageCenter.y - pdImage.getHeight() / 2F;
			contentStream.drawImage(pdImage, textX, textY, pdImage.getWidth() * scale, pdImage.getHeight() * scale);

			contentStream.close();

			return page;
		} catch (IOException e) {
			log.error("Can't create page : " + e.getMessage());
		}

		return page;
	}

	/**
	 * Create cover page, A4 size, portrait.
	 * 
	 * @param document
	 * @return
	 */
	public static PDPage createCoverPage(PDDocument document) {
		PDPage page = new PDPage(PDFWorkbookService.PAGE_SIZE);

		try {
			PDImageXObject pdImage = PDImageXObject.createFromFile("D:/FIF/image.jpg", document);
			PDPageContentStream contentStream = new PDPageContentStream(document, page);

			Point2D.Float pageCenter = PDFUtil.getCenter(page);

			float scale = 1f;
			float textX = pageCenter.x - pdImage.getWidth() / 2F;
			float textY = pageCenter.y - pdImage.getHeight() / 2F;
			contentStream.drawImage(pdImage, textX, textY, pdImage.getWidth() * scale, pdImage.getHeight() * scale);

			contentStream.close();

			return page;
		} catch (IOException e) {
			log.error("Can't create page : " + e.getMessage());
		}

		return page;
	}

	public static void addWrappedText(PDPage page, PDPageContentStream contentStream) {
		try {
			PDFont pdfFont = PDType1Font.HELVETICA;
			float fontSize = 12;
			float leading = 1.5f * fontSize;

			PDRectangle mediabox = page.getMediaBox();
			float margin = 72;
			float width = mediabox.getWidth() - 2 * margin;
			float startX = mediabox.getLowerLeftX() + margin;
			float startY = mediabox.getUpperRightY() - margin;

			String text = "I am trying to create a PDF file with a lot of text contents in the document. "
					+ "I am using PDFBox. I don't know how to split spring, or insert new line, so I "
					+ "was looking at stackoverflow.com, and here we are, trying to write a new java "
					+ "program for such requirement.";
			List<String> lines = new ArrayList<String>();
			int lastSpace = -1;
			while (text.length() > 0) {
				int spaceIndex = text.indexOf(' ', lastSpace + 1);
				if (spaceIndex < 0)
					spaceIndex = text.length();
				String subString = text.substring(0, spaceIndex);
				float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
				if (size > width) {
					if (lastSpace < 0)
						lastSpace = spaceIndex;
					subString = text.substring(0, lastSpace);
					lines.add(subString);
					text = text.substring(lastSpace).trim();
					lastSpace = -1;
				} else if (spaceIndex == text.length()) {
					lines.add(text);
					text = "";
				} else {
					lastSpace = spaceIndex;
				}
			}

			contentStream.beginText();
			contentStream.setFont(pdfFont, fontSize);
			contentStream.newLineAtOffset(startX, startY);
			for (String line : lines) {
				contentStream.showText(line);
				contentStream.newLineAtOffset(0, -leading);
			}
			contentStream.endText();
			contentStream.close();
		} catch (IOException e) {
			log.error("Can't add wrapped text: " + e.getMessage());
		}
	}

	public static PDPage createLicensePage(PDDocument document) {
		PDPage page = new PDPage(PDFWorkbookService.PAGE_SIZE);
		float x = 80f;
		float y = 780f;

		try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
			contentStream.setFont(PDType1Font.HELVETICA, 12);
			contentStream.beginText();
			contentStream.newLineAtOffset(x, y);
			contentStream.showText("text");
			contentStream.endText();
		} catch (IOException e) {
			log.error("Can't create page : " + e.getMessage());
		}

		return page;
	}

	public static PDDocumentInformation createPDFDocumentInfo(String title, String creator, String subject,
			String keywords, Calendar creationDate) {
		PDDocumentInformation documentInformation = new PDDocumentInformation();
		documentInformation.setTitle(title);
		documentInformation.setCreator(creator);
		documentInformation.setSubject(subject);
		documentInformation.setAuthor(creator);
		documentInformation.setCreationDate(creationDate);
		documentInformation.setModificationDate(creationDate);
		documentInformation.setKeywords(keywords);
		documentInformation.setProducer(creator);

		return documentInformation;
	}

	public static String createRandomPassword(int count) {
		return RandomStringUtils.randomAlphabetic(count);
	}

	/**
	 * Create read only, printable PDF.
	 * 
	 * @param ownerPassword
	 *            admin password
	 * @param userPassword
	 *            user password
	 * @return
	 */
	public static StandardProtectionPolicy createStandardProtectionPolicy(String ownerPassword, String userPassword,
			boolean readOnly) {
		int keyLength = 128;

		AccessPermission ap = new AccessPermission();
		if (readOnly) {
			ap.setReadOnly();
		}

		ap.setCanPrint(true);
		StandardProtectionPolicy spp = new StandardProtectionPolicy(ownerPassword, userPassword, ap);
		spp.setEncryptionKeyLength(keyLength);
		spp.setPermissions(ap);

		return spp;
	}

	public static PDMetadata createXMPMetadata(COSStream cosStream, String title, String creator, String subject,
			Calendar creationDate) throws BadFieldValueException, TransformerException, IOException {
		XMPMetadata xmpMetadata = XMPMetadata.createXMPMetadata();

		// PDF/A-1b properties
		PDFAIdentificationSchema pdfaSchema = xmpMetadata.createAndAddPFAIdentificationSchema();
		pdfaSchema.setPart(1);
		pdfaSchema.setConformance("B");

		// Dublin Core properties
		DublinCoreSchema dublinCoreSchema = xmpMetadata.createAndAddDublinCoreSchema();
		dublinCoreSchema.setTitle(title);
		dublinCoreSchema.addCreator(creator);
		dublinCoreSchema.setDescription(subject);

		// XMP Basic properties
		XMPBasicSchema basicSchema = xmpMetadata.createAndAddXMPBasicSchema();
		basicSchema.setCreateDate(creationDate);
		basicSchema.setModifyDate(creationDate);
		basicSchema.setMetadataDate(creationDate);
		basicSchema.setCreatorTool(creator);

		// Create and return XMP data structure in XML format
		ByteArrayOutputStream xmpOutputStream = null;
		OutputStream cosXMPStream = null;
		try {
			xmpOutputStream = new ByteArrayOutputStream();
			cosXMPStream = cosStream.createOutputStream();
			new XmpSerializer().serialize(xmpMetadata, xmpOutputStream, true);
			cosXMPStream.write(xmpOutputStream.toByteArray());
			return new PDMetadata(cosStream);
		} finally {
			IOUtils.closeQuietly(xmpOutputStream);
			IOUtils.closeQuietly(cosXMPStream);
		}
	}

	// from
	// http://stackoverflow.com/questions/6507124/how-to-center-a-text-using-pdfbox
	public static Point2D.Float getCenter(PDPage page) {
		PDRectangle pageSize = page.getMediaBox();
		boolean landscape = isLandscape(page);
		float pageWidth = landscape ? pageSize.getHeight() : pageSize.getWidth();
		float pageHeight = landscape ? pageSize.getWidth() : pageSize.getHeight();

		return new Point2D.Float(pageWidth / 2F, pageHeight / 2F);
	}

	/**
	 * Get x coordinate for centered text
	 * 
	 * @throws IOException
	 */
	public static float getCenteredX(String text, PDFont font, int fontSize, PDPageContentStream content, PDPage page,
			Point2D.Float offset) throws IOException {
		float textX = 0f;
		content.setFont(font, fontSize);

		// Rotate the text according to the page orientation
		boolean pageIsLandscape = isLandscape(page);
		Point2D.Float pageCenter = getCenter(page);

		// We use the text's width to place it at the center of the page
		float stringWidth = getStringWidth(text, font, fontSize);
		if (pageIsLandscape) {
			textX = pageCenter.x - stringWidth / 2F + offset.x;
		} else {
			textX = pageCenter.x - stringWidth / 2F + offset.x;
		}

		return textX;
	}

	// from
	// http://stackoverflow.com/questions/6507124/how-to-center-a-text-using-pdfbox
	public static float getStringWidth(String text, PDFont font, int fontSize) throws IOException {
		return font.getStringWidth(text) * fontSize / 1000F;
	}

	// from
	// http://stackoverflow.com/questions/6507124/how-to-center-a-text-using-pdfbox
	public static boolean isLandscape(PDPage page) {
		int rotation = page.getRotation();
		final boolean isLandscape;
		if (rotation == 90 || rotation == 270) {
			isLandscape = true;
		} else if (rotation == 0 || rotation == 360 || rotation == 180) {
			isLandscape = false;
		} else {
			log.warn(
					"Can only handle pages that are rotated in 90 degree steps. This page is rotated {%d} degrees. Will treat the page as in portrait format",
					rotation);
			isLandscape = false;
		}
		return isLandscape;
	}

	public static void convertToImage(File originalPdfFilePath, int dpiImage, File saveToFolder,
			boolean isConvertFirstPageOnly) throws IOException {
		try (PDDocument document = PDDocument.load(originalPdfFilePath)) {
			PDFRenderer pdfRenderer = new PDFRenderer(document);
			int upperBound = isConvertFirstPageOnly ? 1 : document.getNumberOfPages();
			String imageName;

			for (int page = 0; page < upperBound; page++) {
				if (isConvertFirstPageOnly) {
					imageName = originalPdfFilePath.getName().substring(0,
							originalPdfFilePath.getName().lastIndexOf('.')) + ".jpg";
				} else {
					imageName = originalPdfFilePath.getName().substring(0,
							originalPdfFilePath.getName().lastIndexOf('.')) + " " + (page + 1) + ".jpg";
				}

				String imageFilePath = saveToFolder.getAbsolutePath() + org.apache.commons.io.IOUtils.DIR_SEPARATOR
						+ imageName;

				BufferedImage bim = pdfRenderer.renderImageWithDPI(page, dpiImage, ImageType.RGB);
				ImageIOUtil.writeImage(bim, imageFilePath, dpiImage);
			}
		}
	}
}
