package com.tawabocah.pdf;

import java.util.List;

import com.tawabocah.domain.ProblemInteger;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WorksheetInteger {
	/**
	 * Problem to be rendered on this page
	 */
	private List<ProblemInteger> listProblems;

}
