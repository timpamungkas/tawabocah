package com.tawabocah.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.schema.AdobePDFSchema;
import org.apache.xmpbox.schema.DublinCoreSchema;
import org.apache.xmpbox.schema.XMPBasicSchema;
import org.apache.xmpbox.xml.XmpSerializer;
import org.springframework.stereotype.Component;

import com.tawabocah.util.PDFUtil;

import lombok.Cleanup;

@Component
public class PDFWorkbookCreator {

	public PDFWorkbookCreator() {
		super();

		System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
	}

	public void create(@NotNull File target, PDPage coverPage, PDPage coverAnswerPage, PDPage licensePage,
			List<PDPage> problemPages, List<PDPage> answerPages, String pdfDocInfoTitle, String pdfDocInfoCreator,
			String pdfDocInfoSubject, String pdfDocInfoKeywords) throws IOException, TransformerException {
		@Cleanup
		PDDocument document = new PDDocument();

		// cover
		if (coverPage != null) {
			document.addPage(coverPage);
		}

		// license
		if (licensePage != null) {
			document.addPage(licensePage);
		}

		// problem, without answer
		for (PDPage problemPage : problemPages) {
			document.addPage(problemPage);
		}

		// answer page
		if (coverAnswerPage != null) {
			document.addPage(coverAnswerPage);
		}

		// answer sheet
		for (PDPage answerPage : answerPages) {
			document.addPage(answerPage);
		}

		// doc info
		PDDocumentInformation docInfo = PDFUtil.createPDFDocumentInfo(pdfDocInfoTitle, pdfDocInfoCreator,
				pdfDocInfoSubject, pdfDocInfoKeywords, Calendar.getInstance());
		document.setDocumentInformation(docInfo);

		// doc metadata
		PDDocumentCatalog catalog = document.getDocumentCatalog();
		XMPMetadata metadata = XMPMetadata.createXMPMetadata();

		AdobePDFSchema pdfSchema = metadata.createAndAddAdobePDFSchema();
		pdfSchema.setKeywords(docInfo.getKeywords());
		pdfSchema.setProducer(docInfo.getProducer());

		XMPBasicSchema basicSchema = metadata.createAndAddXMPBasicSchema();
		basicSchema.setModifyDate(docInfo.getModificationDate());
		basicSchema.setCreateDate(docInfo.getCreationDate());
		basicSchema.setCreatorTool(docInfo.getCreator());
		basicSchema.setMetadataDate(new GregorianCalendar());

		DublinCoreSchema dcSchema = metadata.createAndAddDublinCoreSchema();
		dcSchema.setTitle(docInfo.getTitle());
		dcSchema.addCreator(docInfo.getAuthor());
		dcSchema.setDescription(docInfo.getSubject());

		PDMetadata metadataStream = new PDMetadata(document);
		catalog.setMetadata(metadataStream);

		XmpSerializer serializer = new XmpSerializer();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		serializer.serialize(metadata, baos, false);
		metadataStream.importXMPMetadata(baos.toByteArray());

		// protect doc
		StandardProtectionPolicy spp = PDFUtil.createStandardProtectionPolicy(PDFUtil.createRandomPassword(8),
				StringUtils.EMPTY, true);
		document.protect(spp);

		// save
		document.save(target);
	}
}
