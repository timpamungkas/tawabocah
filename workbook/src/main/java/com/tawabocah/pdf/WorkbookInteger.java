package com.tawabocah.pdf;

import java.util.List;

import com.google.common.collect.Lists;
import com.tawabocah.domain.ProblemInteger;

import lombok.Data;

/**
 * A "workbook" that contains set of pages. Each page contains x problems (2
 * operands, operator, + result). Note that this Workbook is just a raw
 * workbook, that need to be reworked if going to be rendered to PDF. For
 * example, if 1 page contains 15 problems, so calling this method with 7
 * pageSize will generate a workbook that contains exactly only 7 pages. IF the
 * PDF needs to rewrite into 2 x 7 pages (e.g. the first 7 is without answer,
 * while the second 7 with answer key), a method to create PDF needs to be
 * defined.
 * 
 * @see ProblemInteger
 */
@Data
public class WorkbookInteger {

	private List<WorksheetInteger> worksheets;

	public WorkbookInteger() {
		setWorksheets(Lists.newArrayList());
	}

	/**
	 * Helper method for accessing list of worksheets
	 * 
	 * @param index
	 * @param page
	 */
	public void addWorksheetAt(int index, WorksheetInteger page) {
		worksheets.add(index, page);
	}

	/**
	 * Helper method for accessing list of worksheets
	 * 
	 * @param page
	 */
	public void addWorksheet(WorksheetInteger page) {
		worksheets.add(page);
	}

	/**
	 * Helper method for accessing list of worksheets
	 * 
	 * @param index
	 * @return
	 */
	public WorksheetInteger getWorksheetAt(int index) {
		return worksheets.get(index);
	}
}
